//
//  Models.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/25/22.
//

import Foundation


struct Question: Identifiable{
    var id: String = UUID().uuidString
    var question: String
    var options: [String]
}

struct Profile: Identifiable{
    var id: String = UUID().uuidString
    var dbId: String
    var name: String
    var lastName: String
    var age: Int
    var major: String
    var photoUrl: String
    var preferences: [String]
    var recommended: [String]
}

struct Preference: Identifiable{
    var id: String
    var name: String
    var category: String
    var iconUrl: String
}

class User: ObservableObject {
    @Published var uID: String
    @Published var name: String
    @Published var lastName: String
    @Published var email: String
    @Published var birthdate: Date
    @Published var ini: Bool
    @Published var questions: Bool
    @Published var verified: Bool
    @Published var fullIni: Bool?
    @Published var fullQuestions: Bool?
    @Published var fullVerified: Bool?
    init() {
        uID = ""
        name = ""
        lastName = ""
        email = ""
        birthdate = Date()
        ini = false
        questions = false
        verified = false
    }
    func updateFullIni(){
        self.fullIni = self.ini && !self.questions && self.verified
        self.fullQuestions = self.ini && self.questions && self.verified
        self.fullVerified = self.ini && !self.verified
    }
}

class UserProfile {
    var name: String
    var lastName: String
    var email: String
    var birthdate: Date?
    var photoUrl: String
    var ini: Bool
    init() {
        name = ""
        lastName = ""
        email = ""
        birthdate = nil
        photoUrl = ""
        ini = false
    }
    init(name: String, lastName: String, email: String, birthday: Date, photoUrl: String) {
        self.name = name
        self.lastName = lastName
        self.email = email
        self.birthdate = birthday
        self.photoUrl = photoUrl
        ini = true
    }
}

class UserLikes {
    var likes: [(String,Date)]
    init() {
        self.likes = []
    }
    init(likes: [(String,Date)]) {
        self.likes=likes
    }
    func numberOfLikes() -> Int{
        return likes.count
    }
    func likesInPastWeek() -> Int{
        var pastWeekLikes: Int = 0
        let today: Date = Date()
        for like in likes{
            let numberOfDays = Calendar.current.dateComponents([.day], from: like.1, to: today).day
            if numberOfDays! < 7{
                pastWeekLikes += 1
            }
        }
        return pastWeekLikes
    }
}

class Activity {
    var name: String
    var description: String
    var date: Date
    var address: String
    var city: String
    var state: String
    var country: String
    var latitude: Double
    var longitude: Double
    var imageUrl: String
    var phone: String
    var email: String
    var idOwner: String
    var ownerName: String
    var id: String?
    var type: String
    
    init(name: String, description: String, date: Date, address: String, city: String, state: String, country: String, latitude: Double, longitude: Double, imageUrl: String, phone: String, email: String, idOwner: String, ownerName: String, type: String) {
        self.name = name
        self.description = description
        self.date = date
        self.address = address
        self.city = city
        self.state = state
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
        self.imageUrl = imageUrl
        self.phone = phone
        self.email = email
        self.idOwner = idOwner
        self.ownerName = ownerName
        self.type = type
    }
    
    init(name: String, description: String, date: Date, address: String, city: String, state: String, country: String, latitude: Double, longitude: Double, imageUrl: String, phone: String, email: String, idOwner: String, ownerName: String, type: String, id: String) {
        self.name = name
        self.description = description
        self.date = date
        self.address = address
        self.city = city
        self.state = state
        self.country = country
        self.latitude = latitude
        self.longitude = longitude
        self.imageUrl = imageUrl
        self.phone = phone
        self.email = email
        self.idOwner = idOwner
        self.ownerName = ownerName
        self.type = type
        self.id = id
    }
}
