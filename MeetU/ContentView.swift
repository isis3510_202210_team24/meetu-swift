//
//  ContentView.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/9/22.
//

import SwiftUI
import CoreData
import Firebase


struct ContentView: View {
    @Environment(\.managedObjectContext) private var viewContext
    
    @FetchRequest(
        sortDescriptors: [NSSortDescriptor(keyPath: \Item.timestamp, ascending: true)],
        animation: .default)
    private var items: FetchedResults<Item>
    
    @StateObject var user = User()
    @StateObject var fm = FirestoreManager()
    @StateObject var locationManager = LocationManager()
    @EnvironmentObject var networkState: NetworkMonitor
    
    func getQuestionStatus(){
        Task{
        user.questions = await fm.updateQuestionsStatus(userID: user.uID)
        user.updateFullIni()
        }
    }
    
    var body: some View {
        VStack{
            
            if !networkState.status{
                Text("No connection, pay your bill!").foregroundColor(Color("Color3")).font(.headline)
            }
            NavigationView{
                VStack{
                    LogIn()
                        .coordinateSpace(name: "VStack")
                        .navigationBarTitle("")
                        .navigationBarHidden(true)
                    
                    NavigationLink(destination: MainView(), tag: true, selection: $user.fullIni) {
                        EmptyView()
                    }
                    NavigationLink(destination: Questions(), tag: true, selection: $user.fullQuestions) {
                        EmptyView()
                    }
                    NavigationLink(destination: VerifyEmail(), tag: true, selection: $user.fullVerified) {
                        EmptyView()
                    }
                }
                .ignoresSafeArea(.keyboard)
            }
            .navigationViewStyle(StackNavigationViewStyle())
            .environmentObject(user)
            .environmentObject(fm)
            .environmentObject(locationManager)
            .task{
                if networkState.status == true{
                    Auth.auth().addStateDidChangeListener { authF, userF in
                        if authF.currentUser != nil {
                            user.uID = authF.currentUser!.uid
                            user.email = authF.currentUser?.email ?? ""
                            user.ini = true
                            user.verified = authF.currentUser!.isEmailVerified
                            getQuestionStatus()
                        } else {
                            user.ini = false
                            user.updateFullIni()
                            //print("NotloggedIn")
                        }
                    }
                }
                DispatchQueue.global(qos: .userInitiated).async {
                    Task{
                        //print("fetchingQuestions")
                        await fm.fetchAllQuestions()
                        await fm.fetchAllPreferences()
                    }
                }
            }
        }
        
        
        /*
         List {
         ForEach(items) { item in
         NavigationLink {
         Text("Item at \(item.timestamp!, formatter: itemFormatter)")
         } label: {
         Text(item.timestamp!, formatter: itemFormatter)
         }
         }
         .onDelete(perform: deleteItems)
         }
         .toolbar {
         ToolbarItem(placement: .navigationBarTrailing) {
         EditButton()
         }
         ToolbarItem {
         Button(action: addItem) {
         Label("Add Item", systemImage: "plus")
         }
         }
         }
         Text("Select an item")
         */
        
    }
    
    private func addItem() {
        withAnimation {
            let newItem = Item(context: viewContext)
            newItem.timestamp = Date()
            
            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
    
    private func deleteItems(offsets: IndexSet) {
        withAnimation {
            offsets.map { items[$0] }.forEach(viewContext.delete)
            
            do {
                try viewContext.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nsError = error as NSError
                fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
            }
        }
    }
}

private let itemFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateStyle = .short
    formatter.timeStyle = .medium
    return formatter
}()

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}

