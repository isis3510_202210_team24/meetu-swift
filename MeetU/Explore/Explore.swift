//
//  Explore.swift
//  MeetU
//
//  Created by Mario Andrade on 11/03/22.
//

import SwiftUI

struct Photo: Identifiable {
    var id = UUID()
    var name: String
}

let samplePhotos = (1...12).map { Photo(name: "profile-\($0)") }
struct Explore: View {
    @EnvironmentObject var fm: FirestoreManager
    @EnvironmentObject  private var user: User
    @State var profiles: [Profile] = []
    @State var images: [UIImage] = []
    @EnvironmentObject  private var networkState: NetworkMonitor
    @State var indexActual: Int = 0
    @State var showProfile: Bool = false
    @State var profileName: String = ""
    @State var profileLastName: String = ""
    @State var profileAge: Int = 0
    @State var profileMajor: String = ""
    @State var profileUrl: String = ""
    @State var userLikedId: String = ""
    @State var countVisits = 0
    @State var offsetActual: CGFloat = .zero
    @State var offsetUtil: CGFloat = .zero
    @State var ini: Bool = false
    @State var isLiked = false
    @State var enoughVisits = false
    @State var showPopup = false
    @State var offLikeAlert = false
    let placeholder = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/1200px-Placeholder_no_text.svg.png"
    @State var gridLayout: [GridItem] = [ GridItem(), GridItem(), GridItem() ]
    
    func checkLike () -> Bool {
        var liked = false
        for like in fm.userLikes {
            if(like == userLikedId){
                liked = true

            }
        }
        
        return liked
    }
    var body: some View {
        
        ZStack{
            Color("Color1")
                .ignoresSafeArea()
            ScrollViewReader{ proxy in
            let scrollView = ScrollView {
                LazyVGrid(columns: gridLayout, alignment: .center, spacing: 10) {
                    ForEach(Array(fm.profiles.enumerated()), id: \.offset) { index, Profile in
                        let url = Profile.photoUrl
                        let lastName = Profile.lastName
                        let name = Profile.name
                        let age = Profile.age
                        let major = Profile.major
                        let id = Profile.dbId
                        if(id != user.uID){
                            VStack{
                                
                                AsyncImage(
                                    url: URL(string: url) ??  URL(string: placeholder)!,
                                    placeholder: { Image("placeholderUser").resizable() },
                                    image: { Image(uiImage: $0).resizable()
                                    }
                                ) .scaledToFill()
                                    .frame(minWidth: 0, maxWidth: .infinity)
                                    .frame(height: 200)
                                    .cornerRadius(10)
                                    .shadow(color: Color.primary.opacity(0.3), radius: 1)
                                HStack(spacing:0){
                                    Rectangle()
                                        .frame(width: 30, height: 30)
                                        .cornerRadius(8, corners: .topLeft)
                                        .foregroundColor(Color("Color1"))
                                    Rectangle()
                                        .frame(width: 30, height: 30)
                                        .foregroundColor(Color("Color2"))
                                    Rectangle()
                                        .frame(width: 30, height: 30)
                                        .cornerRadius(8,corners: .topRight)
                                        .foregroundColor(Color("Color3"))
                                }
                                .padding(.top, -38.0)
                                .onAppear{
                                    print(id)
                                    print(name)
                                    print(" ")
                                    let last = fm.profiles.count
                                    if(index == last - 1){
                                        if(networkState.status){
                                            Task{
                                                await fm.fetchAllProfiles(networkState: networkState.status, userId: user.uID)
                                                proxy.scrollTo(last, anchor: .top)
                                            }
                                        }
                                    }
                                }
                            }.id(index)
                                .contentShape(Rectangle())
                                .onTapGesture {
                                    if(networkState.status){
                                        profileName = name
                                        profileLastName = lastName
                                        profileAge = age
                                        profileMajor = major
                                        profileUrl = url
                                        userLikedId = id
                                        indexActual = index
                                        offsetUtil = offsetActual
                                        isLiked = checkLike()
                                        countVisits = UserDefaults.standard.integer(forKey: id)
                                        countVisits += 1
                                        UserDefaults.standard.set(countVisits, forKey: id)
                                        //print("Visitas")
                                        //print(countVisits)
                                        //print(showPopup)
                                        if(countVisits >= 7){
                                            enoughVisits = true
                                        }
                                        else{
                                            enoughVisits = false
                                        }
                                        
                                        if(enoughVisits && !isLiked){
                                            showPopup = true
                                        }
                                        else{
                                            showPopup = false
                                        }
                                        showProfile.toggle()
                                    }
                                    else{
                                        offLikeAlert = true
                                    }
                                    
                                    
                                }
                                .task {
                                    if UserDefaults.standard.object(forKey: id) == nil {
                                        UserDefaults.standard.set(0, forKey: id)
                                    }
                                    
                                }
                                .alert(isPresented: $offLikeAlert){
                                    Alert(title: Text("Can't reach that profile"),
                                          message: Text("Try again when you are back online"),
                                          dismissButton: .default(Text("Fine..")))
                                }
                        }
                        
                    }
                }
                .offset(x:.zero, y:0)
                .padding(.all, 10)
                GeometryReader { geometry in
                    let offset = geometry.frame(in: .named("scroll")).minY
                    Color.clear.preference(key: ScrollViewOffsetPreferenceKey.self, value: offset)
                }
            }
            scrollView
                .task {
                    
                    await fm.fetchAllProfiles(networkState: networkState.status, userId: user.uID)
                    
                    
                }
                .navigationBarTitle("")
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
                .onAppear{
                    //proxy.scrollTo(indexActual, anchor: UnitPoint(x:.zero, y:offsetActual))
                    //proxy.scrollTo(indexActual, anchor: .top)
                }
                .coordinateSpace(name: "scroll")
                .onPreferenceChange(ScrollViewOffsetPreferenceKey.self) { value in
                    offsetActual = value
                }
            }
            //}v
            if showProfile {
                ProfilePreview(name: profileName, lastName: profileLastName, age: profileAge, major: profileMajor, url: profileUrl, liked: isLiked, userLikedId: userLikedId, currentVisits: countVisits, showingPopup: showPopup)
                GeometryReader { geometry in
                    Image("backIcon")
                        .padding(.top)
                        .onTapGesture {
                            showProfile = false
                        }
                        .glowBorder(color: .white, lineWidth: 4)
                }
            }
            
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
    }
}

struct ScrollViewOffsetPreferenceKey: PreferenceKey {
    static var defaultValue: CGFloat = .zero
    
    static func reduce(value: inout CGFloat, nextValue: () -> CGFloat) {
        value = nextValue()
        //print("value = \(value)")
    }
    
    typealias Value = CGFloat

}

extension View {
    func cornerRadius(_ radius: CGFloat, corners: UIRectCorner) -> some View {
        clipShape( RoundedCorner(radius: radius, corners: corners) )
    }
    
    @ViewBuilder
    func `if`<Transform: View>(_ condition: Bool, transform: (Self) -> Transform) -> some View {
        if condition { transform(self) }
        else { self }
    }
}


struct RoundedCorner: Shape {
    
    var radius: CGFloat = .infinity
    var corners: UIRectCorner = .allCorners
    
    func path(in rect: CGRect) -> Path {
        let path = UIBezierPath(roundedRect: rect, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        return Path(path.cgPath)
    }
}


