//
//  ProfilePreview.swift
//  MeetU
//
//  Created by Mario Andrade on 24/03/22.
//

import SwiftUI
import SystemConfiguration
import PopupView
import SSSwiftUIGIFView

struct ProfilePreview: View{
    @EnvironmentObject  private var fm: FirestoreManager
    @EnvironmentObject  private var user: User
    @EnvironmentObject  private var networkState: NetworkMonitor
    @State var name = "Name"
    @State var lastName = ""
    @State var age = 99
    @State var major = "Major"
    @State var showProfile = true
    @State var url = ""
    @State var liked = false
    @State var userLikedId = ""
    @State var currentVisits = 0
    @State var showingPopup = false
    @State var shouldShow = false
    @State var offLikeAlert = false
    @State var alertTitle = "Can't give like"
    
    let placeholder = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/1200px-Placeholder_no_text.svg.png"
    
    
    var body: some View {
        ZStack{
            Color("Color1")
                .ignoresSafeArea()
            VStack{
                ZStack{
                    
                    AsyncImage(
                        url: URL(string: url) ??  URL(string: placeholder)!,
                        placeholder: { Image("placeholderUser").resizable()},
                        image: { Image(uiImage: $0).resizable()
                        }
                    ).scaledToFill()
                        .frame(minWidth: 0, maxWidth: .infinity)
                        .cornerRadius(10)
                        .shadow(color: Color.primary.opacity(0.3), radius: 1)
                    
                    GeometryReader { geometry in
                        HStack(alignment: .center, spacing:0){
                            Rectangle()
                                .frame(width: geometry.size.width * 0.3, height: geometry.size.height * 0.13)
                                .cornerRadius(15, corners: .topLeft)
                            
                                .foregroundColor(Color("Color1"))
                            Rectangle()
                                .frame(width: geometry.size.width * 0.3, height: geometry.size.height * 0.13)
                                .foregroundColor(Color("Color2"))
                            Rectangle()
                                .frame(width: geometry.size.width * 0.3, height: geometry.size.height * 0.13)
                                .cornerRadius(15,corners: .topRight)
                                .foregroundColor(Color("Color3"))
                        }
                        .padding(.top, geometry.size.height * 0.87 )
                        .padding(.leading, geometry.size.height * 0.028)
                        HStack{
                            Text(name)
                                .font(Font.system(size:35))
                                .foregroundColor(Color.white)
                            
                                .glowBorder(color: .black, lineWidth: 5)
                            Text(String(age))
                                .font(Font.system(size:26))
                                .foregroundColor(Color.white)
                                .padding(.top, 5.0)
                                .glowBorder(color: .black, lineWidth: 5)
                            
                            
                        }
                        .padding(.leading, 25)
                        .frame(width: geometry.size.width - 50, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, alignment: .leading)
                        .padding(.top,geometry.size.height * 0.70)
                        
                        
                        Text(major)
                            .font(Font.system(size:27))
                            .foregroundColor(Color.white)
                            .multilineTextAlignment(.leading)
                            .padding(.trailing,80)
                            .frame(width: geometry.size.width, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, alignment: .leading)
                            .padding(.top,geometry.size.height * 0.76)
                            .padding(.leading, 25)
                            .glowBorder(color: .black, lineWidth: 5)
                        
                        
                        if liked {
                            if networkState.status{
                                Image("likeFilledIcon")
                                    .padding(.leading,geometry.size.height * 0.44)
                                    .padding(.top)
                                    .glowBorder(color: .white, lineWidth: 4)
                                    .onTapGesture {
                                        fm.deleteUserLike(userLiked: userLikedId, userThatLiked: user.uID)
                                        if let index = fm.userLikes.firstIndex(of: userLikedId) {
                                            fm.userLikes.remove(at: index)
                                            UserDefaults.standard.set(0, forKey: userLikedId)
                                        }
                                        liked.toggle()
                                    }
                            }
                            else{
                                Image("likeFilledIcon")
                                    .padding(.leading,geometry.size.height * 0.44)
                                    .padding(.top)
                                    .glowBorder(color: .white, lineWidth: 4)
                                    .onTapGesture {
                                        offLikeAlert = true
                                        alertTitle = "Can't remove like"
                                    }
                            }
                            
                        }
                        else{
                            if networkState.status{
                                Image("likeIcon")
                                    .padding(.leading,geometry.size.height * 0.44)
                                    .padding(.top)
                                    .glowBorder(color: .white, lineWidth: 4)
                                    .onTapGesture {
                                        liked.toggle()
                                        //print("userId")
                                        //print(userLikedId)
                                        fm.setUserLike(userLiked: userLikedId, userThatLiked: user.uID, dateLiked: Date.now)
                                        fm.userLikes.append(userLikedId)
                                    }
                            }
                            else{
                                Image("likeIcon")
                                    .padding(.leading,geometry.size.height * 0.44)
                                    .padding(.top)
                                    .glowBorder(color: .white, lineWidth: 4)
                                    .onTapGesture {
                                        offLikeAlert = true
                                    }
                            }
                        }
                    }
                }.alert(isPresented: $offLikeAlert){
                    Alert(title: Text(alertTitle),
                          message: Text("Try again when you are back online"),
                          dismissButton: .default(Text("Fine..")))
                }
            }.popup(isPresented: $showingPopup , autohideIn: 5 ){
                VStack{
                    SwiftUIGIFPlayerView(gifName: "popup").frame(width: 60, height: 60, alignment: .center)
                        .cornerRadius(15.0)
                    Text("You have seen this profile a lot of times. Maybe you'll like them ;)")
                        .multilineTextAlignment(.center)
                    
                }.frame(width: 350, height: 130)
                    .background(Color("Color2"))
                    .cornerRadius(30.0)
                    .foregroundColor(.white)
                
            }
        }
    }
}

struct GlowBorder: ViewModifier {
    var color: Color
    var lineWidth: Int
    func body(content: Content) -> some View {
        var newContent = AnyView(content)
        for _ in 0..<lineWidth {
            newContent = AnyView(newContent.shadow(color: color, radius: 1))
        }
        return newContent
    }
}

extension View{
    func glowBorder(color: Color, lineWidth: Int) -> some View{
        self.modifier(GlowBorder(color: color, lineWidth: lineWidth))
    }
}


