//
//  VerifyEmail.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/16/22.
//

import SwiftUI
import Firebase

struct VerifyEmail: View {
    @EnvironmentObject private var networkMonitor: NetworkMonitor
    @State private var send: Bool = false
    @State private var message: String = "The verification email was resent"
    var body: some View {
        ZStack{
            Color("Color1")
                .ignoresSafeArea()
            VStack{
                Spacer()
                Image("email")
                    .resizable()
                    .scaledToFit()
                    .frame(width: 150, height: 150)
                Text("Please verify your email!")
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundColor(Color("Color2"))
                    .multilineTextAlignment(.center)
                    .padding(.top, 20)
                    .padding(.bottom, 20)
                Button("Continue to log in"){
                    if networkMonitor.status{
                        print("verify")
                        do {
                            try Auth.auth().signOut()
                        }
                        catch  {
                            
                        }
                        
                    }
                    else {
                        message = "No connection, try again"
                        send = true
                    }
                }
                .padding(.bottom, 8)
                .buttonStyle(NiceButtonStyle())
                Button("Send email again"){
                    print("resend")
                    if networkMonitor.status{
                        if let user = Auth.auth().currentUser{
                            user.sendEmailVerification { error in
                                //print(error)
                            }
                        }
                        message = "The verification email was resent"
                        send = true
                    }
                    else {
                        message = "No connection, try again"
                        send = true
                    }
                }
                .padding(.bottom, 8)
                .buttonStyle(NiceButtonStyle())
                Button("Delete account"){
                    print("delete account")
                    if networkMonitor.status{
                        do{
                            if let user = Auth.auth().currentUser{
                                user.delete()
                            }
                            try Auth.auth().signOut()
                        }
                        catch{
                            
                        }
                    }
                    else {
                        message = "No connection, try again"
                        send = true
                    }
                }
                .padding(.bottom, 8)
                .buttonStyle(NiceButtonStyle())
                if send{
                    Text(message)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color3"))
                }
                Spacer()
            }
        }
        .coordinateSpace(name: "VStack")
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}


