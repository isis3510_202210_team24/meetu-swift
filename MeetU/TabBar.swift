//
//  TabBar.swift
//  MeetU
//
//  Created by Mario Andrade on 24/03/22.
//

import SwiftUI
import AVFoundation
import AudioToolbox

struct TabBar: View {
    @Binding var selectedIndex: Int
    let icons = [
        "profileIcon",
        "searchIcon",
        "meetIcon",
        "calendarIcon"
    ]
    var mySound: SystemSoundID = 1026;
    
    init(selectedIndex: Binding<Int>){
        if let soundURL = Bundle.main.url(forResource: "tap", withExtension: "caf") {
            AudioServicesCreateSystemSoundID(soundURL as CFURL, &mySound)
        }
        self._selectedIndex = selectedIndex
    }
    
    var body: some View {
            HStack{
                ForEach(0..<3,id: \.self){number in
                    Spacer()
                    if selectedIndex == number {
                        Button(action:{
                            self.selectedIndex = number
                            AudioServicesPlaySystemSound(mySound)
                            AudioServicesPlaySystemSound(kSystemSoundID_Vibrate)
                        }, label:{Image(icons[number])
                                .frame(width: 80, height: 50)
                                .background(Color("Color3"))
                                .foregroundColor(.black)
                                .cornerRadius(15)
                        })
                    }
                    else{
                        Button(
                            action:{self.selectedIndex = number
                            AudioServicesPlaySystemSound(mySound)
                        }, label:{Image(icons[number])
                                .frame(width: 80, height: 50)
                                .foregroundColor(.black)
                        })
                    }
                    Spacer()
                }
            }.frame(height: 50)
            .background(Color("Color1"))
        }
}


