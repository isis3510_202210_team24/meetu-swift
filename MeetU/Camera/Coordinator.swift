//
//  Coordinator.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/25/22 inspired by https://iosapptemplates.com/blog/swiftui/photo-camera-swiftui
//

import Foundation
import SwiftUI
class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @Binding var isCoordinatorShown: Bool
    @Binding var imageInCoordinator: Image?
    @Binding var rawImageInCoordinator: UIImage?
    init(isShown: Binding<Bool>, image: Binding<Image?>, rawImage: Binding<UIImage?>) {
        _isCoordinatorShown = isShown
        _imageInCoordinator = image
        _rawImageInCoordinator = rawImage
    }
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let unwrapImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
        rawImageInCoordinator = unwrapImage
        imageInCoordinator = Image(uiImage: unwrapImage)
        isCoordinatorShown = false
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        isCoordinatorShown = false
    }
}
