//
//  FirestoreManager.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/25/22.
//

import Foundation
import Firebase
import UIKit


class FirestoreManager: ObservableObject {
    
    @Published var questions = [Question]()
    @Published var preferences = [Preference]()
    @Published var profiles = [Profile]()
    @Published var userLikes = [String]()
    @Published var userProfile = UserProfile()
    @Published var userFullLikes = UserLikes()
    @Published var activities = [Activity]()
    @Published var likeCount: Int?
    
    private var likesDict = [String:String]()
    private var db = Firestore.firestore()
    private var storage = Storage.storage()
    private var lastProfileVis: DocumentSnapshot?
    
    // Cache
    private let userProfileCache = NSCache<NSString, UserProfile>()
    private let userLikesCache = NSCache<NSString, UserLikes>()
    private let activitiesCache = NSCache<NSString, NSArray>()
    private let profilesCache = NSCache<NSString, NSArray>()
    
    func fetchAllQuestions() {
        db.collection("questions").addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                //print("No questions")
                return
            }
            
            let newQuestions = documents.map { queryDocumentSnapshot -> Question in
                let data = queryDocumentSnapshot.data()
                let question = data["question"] as? String ?? ""
                let options = data["options"] as? [String] ?? [""]
                return Question(question: question, options: options)
            }
            
            DispatchQueue.main.async {
                self.questions = newQuestions
                //print("fetchedQuestions")
            }
        }
    }
    
    func fetchAllProfiles(networkState : Bool, userId : String) async {
        
        if networkState == true {
            var query = db.collection("profiles").limit(to: 50)
            
            if(self.profiles.count > 0){
                query = query.limit(to: self.profiles.count + 50)
                //query.start(afterDocument: lastProfileVis!)
            }
            query.addSnapshotListener { (querySnapshot, error) in
                guard let documents = querySnapshot?.documents else {
                    //print("No profiles")
                    return
                }
                self.lastProfileVis = documents.last
                self.profiles = documents.map { queryDocumentSnapshot -> Profile in
                    
                    let data = queryDocumentSnapshot.data()
                    let name = data["name"] as? String ?? ""
                    let lastName = data["lastName"] as? String ?? ""
                    let dbId = queryDocumentSnapshot.documentID
                    let age = data["age"] as? Int ?? 0
                    let major = data["major"] as? String ?? ""
                    let photoUrl = data["photoUrl"] as? String ?? ""
                    let preferences = self.fetchProfilePreferences(profileId: queryDocumentSnapshot.documentID)
                    let recommended = self.fetchProfileRecommended(profileId: queryDocumentSnapshot.documentID)
                    return Profile(dbId: dbId,name: name,lastName: lastName, age: age, major: major, photoUrl: photoUrl, preferences: preferences, recommended: recommended)
                }
            }
            if let profilesCache = profilesCache.object(forKey: "profiles") as? [Profile]{
                print("profiles are already in cache" + String(profilesCache.count) )
            }
            else{
                profilesCache.setObject(profiles as NSArray, forKey: "profiles")
            }
        }
        else {
            if let profilesCache = profilesCache.object(forKey: "profiles") as? [Profile] {
                profiles = profilesCache
            }
            else {
                profiles = []
            }
        }
    }
    
    func fetchAllPreferences() {
        db.collection("preferences").addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                //print("No questions")
                return
            }
            
            let newPreferences = documents.map { queryDocumentSnapshot -> Preference in
                let data = queryDocumentSnapshot.data()
                let name = data["name"] as? String ?? ""
                let category = data["category"] as? String ?? ""
                let iconUrl = data["iconUrl"] as? String ?? ""
                return Preference(id: queryDocumentSnapshot.documentID, name: name, category: category, iconUrl: iconUrl)
            }
            
            DispatchQueue.main.async {
                self.preferences = newPreferences
                //print("fetchedQuestions")
            }
        }
    }
    
    
    func fetchProfilePreferences(profileId : String) -> [String] {
        var newPreferences: [String] = []
        let docRef = db.collection("userPreferences").document(profileId)
        
        docRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data()
                newPreferences = dataDescription?["preferences"] as? [String] ?? []
            } else {
                print("Document does not exist")
            }
        }
        return newPreferences
    }
    
    func fetchProfileRecommended(profileId: String ) ->[String] {
        var recommended: [String] = []
        db.collection("profiles").document(profileId).collection("recommendedProfiles").addSnapshotListener { (querySnapshot, error) in
            guard let documents = querySnapshot?.documents else {
                //print("No recommended profiles for this user")
                return
            }
            recommended = documents.map { queryDocumentSnapshot -> String in
                
                let data = queryDocumentSnapshot.data()
                let profileId = data["id"] as? String ?? ""
                return profileId
            }
        }
        return recommended
    }
    
    func fetchUserLikes(userId : String) async {
        do{
            let user = try await db.collection("likes").document(userId).getDocument().data()
            var likes: [String] = []
            let usersLiked = user?["usersLiked"] as? [Any] ?? []
            for like in usersLiked {
                let likedId = like as? [String:Any] ?? [:]
                let strId = likedId["userLikedId"] as? String ?? ""
                let date = likedId["date"] as? String ?? ""
                likes.append(strId)
                let allLikes = likes
                
                DispatchQueue.main.async {
                    self.userLikes = allLikes
                    self.likesDict[strId] = date
                }
                
                print("likes retrieved")
                
            }
        }
        catch{
            print("Can't fetch user likes")
        }
    }
    
    func fetchUserLikeCount(networkState: Bool, userId : String) async {
        if networkState{
            do{
                let user = try await db.collection("likes").document(userId).getDocument().data()
                
                DispatchQueue.main.async {
                    self.likeCount = user?["likes"] as? Int
                }
                print("likes count retrieved")
            }
            catch{
                print("Can't fetch user likes")
            }
        }
    }
    
    func fetchNameById(userId : String) async -> String {
        var fullName = ""
        do{
            let profile = try await db.collection("profiles").document(userId).getDocument().data()
            let name = profile?["name"] as? String ?? ""
            let lastName = profile?["lastName"] as? String ?? ""
            fullName =  name + " " + lastName
        }
        catch{
            print("cant fetch name of profile")
        }
        return fullName
    }
    
    func setUserLike(userLiked: String,userThatLiked: String, dateLiked: Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let data: [String: Any] = [
            "usersLiked": FieldValue.arrayUnion([["date": dateFormatter.string(from: dateLiked), "userLikedId": userLiked]])
        ]
        db.collection("likes").document(userThatLiked).updateData(data){ err in
            if let err = err {
                print("Error writing like: \(err)")
            } else {
                //print("Like successfully written!")
            }
        }
        db.collection("likes").document(userLiked).updateData(["likes":FieldValue.increment(Int64(1))])
    }
    
    func deleteUserLike(userLiked: String, userThatLiked: String){
        let date = likesDict[userLiked]
        let data: [String: Any] = [
            "usersLiked": FieldValue.arrayRemove([["date": date, "userLikedId": userLiked]])
        ]
        db.collection("likes").document(userThatLiked).updateData(data){ err in
            if let err = err {
                print("Error deleting like: \(err)")
            } else {
                //print("Like successfully deleted!")
            }
        }
        db.collection("likes").document(userLiked).updateData(["likes":FieldValue.increment(Int64(-1))])
    }
    
    func setUserQuestions(user: String, questions: [[String]]){
        var responses: [[String:String]] = []
        for quest in questions{
            responses.append(["question": quest[0], "answer": quest[1]])
        }
        let data: [String: [[String:String]]] = ["questionsAnswered":responses]
        db.collection("questionsAnswered").document(user).setData(data){ err in
            if let err = err {
                print("Error writing questions like: \(err)")
            } else {
                //print("Qustions successfully written!")
            }
        }
        
    }
    
    func setUserPreferences(user: String, preferences: [String]){
        let data: [String:[String]] = ["preferences":preferences]
        db.collection("userPreferences").document(user).setData(data){ err in
            if let err = err {
                print("Error writing preferences: \(err)")
            } else {
                //print("Qustions successfully written!")
            }
        }
        
    }
    
    func setRecommended(user: String){
        db.collection("recommended").document(user).setData(["profiles":[]]){ err in
            if let err = err {
                print("Error writing recommended like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
        
    }
    
    func setUser(user: String, userData: User){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let data: [String:String] = ["email":userData.email,"birthDate":dateFormatter.string(from: userData.birthdate)]
        db.collection("users").document(user).setData(data){ err in
            if let err = err {
                print("Error writing user like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
        
    }
    
    func setProfile(user: String, userData: User){
        let diffComponents = Calendar.current.dateComponents([.year], from: userData.birthdate, to: Date()).year
        let data: [String:Any] = ["name":userData.name,"lastName":userData.lastName,"age":diffComponents ?? 0, "questions": true, "major":""]
        db.collection("profiles").document(user).setData(data){ err in
            if let err = err {
                print("Error writing user like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
    }
    
    func setLikes(user: String){
        let data: [String:Any] = ["likes":0,"usersLiked":[]]
        db.collection("likes").document(user).setData(data){ err in
            if let err = err {
                print("Error writing user like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
    }
    
    func updateActivityStats(userId: String, dateCreated : Date){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let data: [String: Any] = [
            "usersThatCreated": FieldValue.arrayUnion([["date": dateFormatter.string(from: dateCreated), "userId": userId]])
        ]
        db.collection("createActivityUsage").document("statistics").updateData(data){ err in
            if let err = err {
                print("Error writing user like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
    }
    
    func updateUser(user: String, userData: [String:String]){
        db.collection("users").document(user).updateData(userData){ err in
            if let err = err {
                print("Error writing user like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
    }
    
    func updateProfile(user: String, userData: [String:String]){
        db.collection("profiles").document(user).updateData(userData){ err in
            if let err = err {
                print("Error writing user like: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
    }
    func updateActivity(activity: String, activityData: [String:String]){
        db.collection("activities").document(activity).updateData(activityData){ err in
            if let err = err {
                print("Error writing activity: \(err)")
            } else {
                //print("User successfully written!")
            }
        }
    }
    
    func setImage(user: String, image: Data){
        let storageRef = storage.reference()
        
        let imageRef = storageRef.child("images/"+user+".jpeg")
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        imageRef.putData(image, metadata: metadata) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            imageRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
                self.updateProfile(user: user, userData: ["photoUrl": downloadURL.absoluteString])
            }
        }
    }
    
    func setImageActivity(activity: String, image: Data){
        
        let storageRef = storage.reference()
        
        let imageRef = storageRef.child("imagesActivity/"+activity+".jpeg")
        
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        
        imageRef.putData(image, metadata: metadata) { (metadata, error) in
            guard let metadata = metadata else {
                return
            }
            imageRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    return
                }
                self.updateActivity(activity: activity, activityData: ["imageUrl": downloadURL.absoluteString])
            }
        }
    }
    
    func refreshQuestionsStatus(userID: String){
        db.collection("profiles").document(userID).updateData(["questions":false])
    }
    
    func updateQuestionsStatus(userID: String) async -> Bool{
        do{
            let profile = try await db.collection("profiles").document(userID).getDocument().data()
            let status =  profile?["questions"] as? Bool ?? false
            return status
        }
        catch {
            return false
        }
    }
    
    func fetchFullProfile(networkState : Bool, userID: String) async {
        print("fetchFullProfile")
        if networkState == true {
            do{
                let profile = try await db.collection("profiles").document(userID).getDocument().data()
                let user = try await db.collection("users").document(userID).getDocument().data()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd"
                let newUserProfile = UserProfile(
                    name: profile?["name"] as? String ?? "-",
                    lastName: profile?["lastName"] as? String ?? "-",
                    email: user?["email"] as? String ?? "-",
                    birthday: dateFormatter.date(from: user?["birthDate"] as? String ?? "") ?? Date(),
                    photoUrl: profile?["photoUrl"] as? String ?? ""
                )
                DispatchQueue.main.async {
                    self.userProfile = newUserProfile
                }
                userProfileCache.setObject(newUserProfile as UserProfile, forKey: "profile")
            }
            catch {
                //print(error)
            }
        }
        
        else {
            if let profileCache = userProfileCache.object(forKey: "profile"){
                DispatchQueue.main.async {
                    self.userProfile = profileCache
                }
            }
            else {
                //print("there is no profile in cache")
            }
        }
    }
    
    func fetchFullLikes(networkState : Bool, userID: String) async {
        if networkState == true {
            do{
                let likes = try await db.collection("likes").document(userID).getDocument().data()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd"
                let usersLiked = likes?["usersLiked"] as? [Any] ?? []
                var newLikes:[(String,Date)] = []
                for like in usersLiked {
                    let likedId = like as? [String:Any] ?? [:]
                    let id = likedId["userLikedId"] as? String ?? ""
                    let date = likedId["date"] as? String ?? ""
                    newLikes.append((id,dateFormatter.date(from: date) ?? Date()))
                }
                
                let newUserLikes = UserLikes(
                    likes: newLikes
                )
                DispatchQueue.main.async {
                    self.userFullLikes = newUserLikes
                }
                userLikesCache.setObject(newUserLikes as UserLikes, forKey: "userLikes")
            }
            catch {
                //print(error)
            }
        }
        
        else {
            if let likesCache = userLikesCache.object(forKey: "userLikes"){
                DispatchQueue.main.async {
                    self.userFullLikes = likesCache
                }
            }
            else {
                //print("there is no profile in cache")
            }
        }
    }
    
    func addActivity(networkState : Bool, activity: Activity) -> String{
        var docId = ""
        if networkState == true{
            print("addingActivity...")
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy/MM/dd-HH:mm:ss Zzzz"
            var ref: DocumentReference? = nil
            ref = db.collection("activities").addDocument(data: ["name": activity.name,
                                                                 "description": activity.description,
                                                                 "date": dateFormatter.string(from: activity.date),
                                                                 "address": activity.address,
                                                                 "city": activity.city,
                                                                 "state": activity.state,
                                                                 "country": activity.country,
                                                                 "latitude": activity.latitude,
                                                                 "longitude": activity.longitude,
                                                                 "imageUrl": activity.imageUrl,
                                                                 "phone": activity.phone,
                                                                 "email": activity.email,
                                                                 "idOwner": activity.idOwner,
                                                                 "ownerName": activity.ownerName,
                                                                 "type": activity.type
                                                                ]
            ){ err in
                if let err = err {
                    print("Error adding document: \(err)")
                } else {
                    print("Document added with ID: \(ref!.documentID)")
                }
            }
            docId = ref?.documentID ?? ""
        }
        return docId
    }
    
    func deleteActivity(id: String){
        db.collection("activities").document(id).delete()
    }
    
    func fetchActivities(networkState : Bool) async {
        if networkState == true {
            do{
                let activities = try await db.collection("activities").getDocuments().documents
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy/MM/dd-HH:mm:ss Zzzz"
                var newActivities = [Activity]()
                for activity in activities {
                    let data = activity.data()
                    let newActivity = Activity(name: data["name"] as? String ?? "",
                                               description: data["description"] as? String ?? "",
                                               date: dateFormatter.date(from: data["date"] as? String ?? "") ?? Date(),
                                               address: data["address"] as? String ?? "",
                                               city: data["city"] as? String ?? "",
                                               state: data["state"] as? String ?? "",
                                               country: data["country"] as? String ?? "",
                                               latitude: data["latitude"] as? Double ?? 0,
                                               longitude: data["longitude"] as? Double ?? 0,
                                               imageUrl: data["imageUrl"] as? String ?? "",
                                               phone: data["phone"] as? String ?? "",
                                               email: data["email"] as? String ?? "",
                                               idOwner: data["idOwner"] as? String ?? "",
                                               ownerName: data["ownerName"] as? String ?? "",
                                               type: data["type"] as? String ?? "",
                                               id: activity.documentID
                    )
                    newActivities.append(newActivity)
                }
                activitiesCache.setObject(newActivities as NSArray, forKey: "activities")
                let mainActorActivities = newActivities
                DispatchQueue.main.async {
                    self.activities =  mainActorActivities
                }
            }
            catch {
                DispatchQueue.main.async {
                    self.activities = []
                }
            }
        }
        else {
            if let allActivities = activitiesCache.object(forKey: "activities") as? [Activity] {
                DispatchQueue.main.async {
                    self.activities = allActivities
                }
            }
            else {
                DispatchQueue.main.async {
                    self.activities = []
                }
            }
        }
    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    func logOut(){
        profiles = [Profile]()
        userLikes = [String]()
        userProfile = UserProfile()
        userFullLikes = UserLikes()
        
        do{
            try Auth.auth().signOut()
        }
        catch{
            //print("CatchAuth")
            //print(error)
        }
        
        userProfileCache.removeAllObjects()
        userLikesCache.removeAllObjects()
        activitiesCache.removeAllObjects()
        profilesCache.removeAllObjects()
        DispatchQueue.global(qos: .background).async {
            self.resetDefaults()
        }
    }
    
}
