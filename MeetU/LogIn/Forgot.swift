//
//  Forgot.swift
//  MeetU
//
//  Created by Andrés Martínez on 4/3/22.
//

import SwiftUI
import Firebase

struct Forgot: View {
    @State var username: String = ""
    @State private var action: Int? = 0
    @State private var resent: Bool = false
    @State private var textoError: String = "Better luck next time :("
    @EnvironmentObject private var networkState: NetworkMonitor
    
    let onHome: () -> Void
    
    func reset(){
        username = ""
    }
    
    func changeMessage(message: String){
        textoError = message
    }
    
    var body : some View {
        GeometryReader { geometry in
            VStack {
                Image("logo1").resizable().scaledToFit().frame(height: 50)
                
                if action == 0 || action == 2{
                    Text("Recover Password")
                        .font(.title)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color2"))
                    TextField("Enter email", text: $username)
                        .textFieldStyle(NiceTextFieldStyle())
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .padding()
                        .textContentType(.emailAddress)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                        .submitLabel(.done)
                    
                    
                    Button("Send email"){
                        forgot()
                    }
                    .padding()
                    .frame(width: 140, height: 40)
                    .background(Color("Color3"))
                    .clipShape(Capsule())
                    .foregroundColor(Color.white)
                    
                    
                }
                else if action == 1{
                    Text("You have an email!")
                        .font(.title)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color2"))
                        .padding(.bottom)
                    
                    
                    Text("We sent you an email to:")
                        .font(Font.custom("Fredoka One", size: 20))
                        .padding(.bottom, 25.0)
                        .multilineTextAlignment(.center)
                        .foregroundColor(Color("Color2"))
                    
                    VStack{
                        RoundedRectangle(cornerRadius: 10.0)
                            .frame( width:280,height: 40.0, alignment: .center)
                            .ignoresSafeArea().foregroundColor(Color("Color2"))
                            .overlay(Text(username).accentColor(.black))
                    }
                    .padding(.bottom, 30.0)
                    
                    
                    Text("Check your email.")
                        .font(Font.custom("Fredoka One", size: 19))
                        .multilineTextAlignment(.center)
                        .padding(.bottom, 40.0)
                        .foregroundColor(Color("Color2"))
                    
                    if !resent{
                        Text("You did not get it?")
                            .font(Font.custom("Fredoka One", size: 19))
                            .foregroundColor(Color("Color2"))
                    }
                    
                    if resent{
                        Text("Email sent")
                            .font(Font.custom("Fredoka One", size: 19))
                            .foregroundColor(Color("Color3"))
                    }
                    else{
                        Button(action: {
                            resent = true
                            forgot()
                        },label:{
                            Text("Request again")
                                .font(Font.custom("Fredoka One", size: 19))
                                .foregroundColor(Color("Color3"))
                        })
                    }
                    
                    
                    
                }
                Button(action: {self.onHome()},label: {Text("Return")})
                    .padding()
                    .frame(width: 140, height: 40)
                    .background(Color("Color3"))
                    .clipShape(Capsule())
                    .foregroundColor(Color.white)
                
                if self.action == 2 {
                    Text(textoError)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color3"))
                }
                
                Spacer()
                
            }.frame(width: geometry.size.width, height: geometry.size.height)
        }
    }
    
    func forgot(){
        if username.count == 0{
            changeMessage(message: "Please write your email")
            self.action = 2
        }
        else if username.count<5{
            changeMessage(message: "The email is too short")
            self.action = 2
        }
        else if !networkState.status{
            changeMessage(message: "No internet connection")
            self.action = 2
        }
        else{
            Auth.auth().sendPasswordReset(withEmail: self.username) { error in
                if error != nil {
                    if let errCode = AuthErrorCode(rawValue: error!._code){
                        switch errCode {
                        case .invalidEmail:
                            changeMessage(message: "Invalid email")
                        case .wrongPassword:
                            changeMessage(message: "Wrong password")
                        default:
                            changeMessage(message: error?.localizedDescription ?? "Better luck next time :(")
                        }
                    }
                    self.action = 2
                } else {
                    self.action = 1
                }
            }
        }
    }
}

