//
//  LogInForm.swift
//  MeetU
//
//  Created by Andrés Martínez on 4/3/22.
//

import SwiftUI
import Firebase

struct LogInForm: View {
    @EnvironmentObject var fm: FirestoreManager
    @EnvironmentObject  private var networkState: NetworkMonitor
    @EnvironmentObject var user: User
    @State var username: String = ""
    @State var password: String = ""
    @State private var action: Int? = 0
    @State var field1 = ""
    @State var field2 = ""
    @State private var textoError: String = "Better luck next time :("
    @FocusState var focus: Fields?
    
    let onRegister: () -> Void
    let onForgot: () -> Void
    let updateUser: () -> Void
    
    func reset(){
        password = ""
    }
    
    var body : some View {
        GeometryReader { geometry in
            VStack {
                Group{
                    Image("logo1").resizable().scaledToFit().frame(height: 50)
                    Text("Log In")
                        .font(.title)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color2"))
                    
                    HStack{
                        Text("Username")
                            .font(.subheadline)
                            .fontWeight(.light)
                            .foregroundColor(Color("Color2"))
                            .padding(.top)
                        Spacer()
                    }
                    .padding(.leading,geometry.size.width * 0.05)
                    TextField("", text: $username)
                    //.background(Color("Color1"), in: Capsule())
                        .textFieldStyle(NiceTextFieldStyle())
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .textContentType(.emailAddress)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                        .submitLabel(.next)
                        .focused($focus, equals: .field1)
                        .onSubmit {
                            switch focus {
                            case .field1:
                                focus = .field2
                            default:
                                focus = nil
                            }
                        }
                    
                    HStack{
                        Text("Password")
                            .font(.subheadline)
                            .fontWeight(.light)
                            .foregroundColor(Color("Color2"))
                            .padding(.top)
                        Spacer()
                    }
                    .padding(.leading,geometry.size.width * 0.05)
                    SecureField("", text: $password)
                        .textFieldStyle(NiceTextFieldStyle())
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .padding(.bottom,5)
                        .textContentType(.password)
                        .keyboardType(.default)
                        .autocapitalization(.none)
                        .submitLabel(.done)
                        .focused($focus, equals: .field2)
                        .onSubmit {
                            switch focus {
                            case .field2:
                                focus = nil
                            default:
                                focus = nil
                            }
                        }
                    
                    Button(action: {onForgot()},label:{Text("Forgot Password?")})
                        .padding(.bottom)
                    
                    Button("Log In") {
                        self.action = 3
                        login()
                    }
                    .padding()
                    .frame(width: 140, height: 40)
                    .background(Color("Color3"))
                    .clipShape(Capsule())
                    .foregroundColor(Color.white)
                    
                    
                    
                    Button(action: {self.onRegister()},label: {Text("Register")})
                        .padding()
                        .frame(width: 140, height: 40)
                        .background(Color("Color3"))
                        .clipShape(Capsule())
                        .foregroundColor(Color.white)
                }.onTapGesture(perform: {
                
                    focus = nil
                })
                if self.action == 2 {
                    Text(textoError)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color3"))
                }
                
                /*
                 NavigationLink(destination: MainView(), tag: 1, selection: $action) {
                 EmptyView()
                 }
                 */
                
                Spacer()
            }.frame(width: geometry.size.width, height: geometry.size.height)
                .disabled(self.action==3)
        }
    }
    
    func changeMessage(message: String){
        textoError = message
    }
    
    func login(){
        if username.count == 0{
            changeMessage(message: "Please write your email")
            self.action = 2
        }
        else if username.count<5{
            changeMessage(message: "The email is too short")
            self.action = 2
        }
        else if password.count==0{
            changeMessage(message: "Please write your password")
            self.action = 2
        }
        else if password.count<8{
            changeMessage(message: "The password is too short")
            self.action = 2
        }
        else if !networkState.status{
            changeMessage(message: "No internet connection")
            self.action = 2
        }
        else{
            Auth.auth().signIn(withEmail: username, password: password) { (result, error) in
                if error != nil {
                    if let errCode = AuthErrorCode(rawValue: error!._code){
                        switch errCode {
                        case .invalidEmail:
                            changeMessage(message: "Invalid email")
                        case .wrongPassword:
                            changeMessage(message: "Wrong password")
                        default:
                            changeMessage(message: error?.localizedDescription ?? "Better luck next time :(")
                        }
                    }
                    self.action = 2
                } else {
                    self.action = 1
                    updateUser()
                }
            }
        }
    }
    
}

struct NiceTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<_Label>) -> some View {
        configuration
            .multilineTextAlignment(.center)
            .foregroundColor((Color("Color1")))
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color("Color2"))
                    .frame(height: 30)
            )
            
            //.textFieldStyle(.roundedBorder)
            //.textFieldStyle(RoundedBorderTextFieldStyle())
    }
}
