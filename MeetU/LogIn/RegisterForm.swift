//
//  RegisterForm.swift
//  MeetU
//
//  Created by Andrés Martínez on 4/3/22.
//

import SwiftUI
import Firebase

struct RegisterForm: View {
    
    @State var name: String = ""
    @State var lastName: String = ""
    @State var username: String = ""
    @State var password: String = ""
    @State var password2: String = ""
    @State private var action: Int? = 0
    @State private var birthDate: Date = Date()
    @State private var textoError: String = "Better luck next time :("
    @EnvironmentObject private var networkState: NetworkMonitor
    @FocusState var focus: Fields?
    @EnvironmentObject var user: User
    
    
    let decimalCharacters = CharacterSet.decimalDigits
    let dateFormatter = DateFormatter()
    
    
    let onHome: () -> Void
    let createUser: () -> Void
    
    
    
    var dateClosedRange: ClosedRange<Date> {
        let min = Calendar.current.date(byAdding: .year, value: -99, to: Date())!
        let max = Calendar.current.date(byAdding: .year, value: -18, to: Date())!
        return min...max
    }
    
    func reset(){
        username = ""
        password = ""
        password2 = ""
        name = ""
        lastName = ""
        birthDate = Date()
    }
    
    func changeMessage(message: String){
        textoError = message
    }
    
    
    var body : some View {
        GeometryReader { geometry in
            VStack {
                Image("logo1").resizable().scaledToFit().frame(height: 40)
                
                Group{
                    Text("Register")
                        .font(.title)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color2"))
                        .padding(.bottom,23)
                    HStack{
                        Text("Name")
                            .font(.subheadline)
                            .fontWeight(.light)
                            .foregroundColor(Color("Color2"))
                            .padding(.top,5)
                        Spacer()
                    }
                    .padding(.leading,geometry.size.width * 0.05)
                    TextField("", text: $name)
                        .textFieldStyle(NiceTextFieldStyle())
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .textContentType(.givenName)
                        .keyboardType(.alphabet)
                        .submitLabel(.next)
                        .focused($focus, equals: .field1)
                    
                    HStack{
                        Text("Last Name")
                            .font(.subheadline)
                            .fontWeight(.light)
                            .foregroundColor(Color("Color2"))
                            .padding(.top,8)
                        Spacer()
                    }
                    .padding(.leading,geometry.size.width * 0.05)
                    TextField("", text: $lastName)
                        .textFieldStyle(NiceTextFieldStyle())
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .textContentType(.familyName)
                        .keyboardType(.alphabet)
                        .submitLabel(.next)
                        .focused($focus, equals: .field2)
                    
                    HStack{
                        Text("Email")
                            .font(.subheadline)
                            .fontWeight(.light)
                            .foregroundColor(Color("Color2"))
                            .padding(.top,8)
                        Spacer()
                    }
                    .padding(.leading,geometry.size.width * 0.05)
                    TextField("", text: $username)
                        .textFieldStyle(NiceTextFieldStyle())
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .textContentType(.emailAddress)
                        .keyboardType(.emailAddress)
                        .autocapitalization(.none)
                        .submitLabel(.next)
                        .focused($focus, equals: .field3)
                    
                    Group{
                        HStack{
                            Text("Password")
                                .font(.subheadline)
                                .fontWeight(.light)
                                .foregroundColor(Color("Color2"))
                                .padding(.top,8)
                            Spacer()
                        }
                        .padding(.leading,geometry.size.width * 0.05)
                        SecureField("", text: $password)
                            .textFieldStyle(NiceTextFieldStyle())
                            .frame(width: geometry.size.width * 0.9, height: nil)
                            .textContentType(.newPassword)
                            .keyboardType(.default)
                            .autocapitalization(.none)
                            .submitLabel(.done)
                            .focused($focus, equals: .field4)
                        
                        HStack{
                            Text("Re-enter password")
                                .font(.subheadline)
                                .fontWeight(.light)
                                .foregroundColor(Color("Color2"))
                                .padding(.top,8)
                            Spacer()
                        }
                        .padding(.leading,geometry.size.width * 0.05)
                        SecureField("", text: $password2)
                            .textFieldStyle(NiceTextFieldStyle())
                            .frame(width: geometry.size.width * 0.9, height: nil)
                            .textContentType(.newPassword)
                            .keyboardType(.default)
                            .autocapitalization(.none)
                            .submitLabel(.done)
                            .focused($focus, equals: .field5)
                            .keyboardAdaptive()
                    }
                    DatePicker("Birthdate", selection: $birthDate, in: dateClosedRange, displayedComponents: .date)
                        .font(.subheadline)
                        .frame(width: geometry.size.width * 0.9, height: nil)
                        .colorInvert()
                        .colorMultiply(Color("Color2"))
                        .submitLabel(.next)
                        .focused($focus, equals: .field6)
                        .padding(.bottom, 15)
                        .padding(.top, 8)
                    
                }.onSubmit {
                    switch focus {
                    case .field1:
                        focus = .field2
                    case .field2:
                        focus = .field3
                    case .field3:
                        focus = .field4
                    case .field4:
                        focus = .field5
                    case .field5:
                        focus = .field6
                    case .field6:
                        focus = nil
                    default:
                        focus = nil
                    }
                }
                .onTapGesture(perform: {
                    focus = nil
                })
                
                Button(action: {
                    self.action = 4
                    self.register()
                    //self.action=1
                }
                       ,label: {Text("Register")})
                .padding()
                .frame(width: 140, height: 40)
                .background(Color("Color3"))
                .clipShape(Capsule())
                .foregroundColor(Color.white)
                Button(action: {self.onHome()},label: {Text("Back")})
                    .padding()
                    .frame(width: 140, height: 40)
                    .background(Color("Color3"))
                    .clipShape(Capsule())
                    .foregroundColor(Color.white)
                
                if self.action == 2 {
                    Text(textoError)
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color3"))
                }
                
                /*
                 NavigationLink(destination: Questions(), tag: 1, selection: $action) {
                 EmptyView()
                 }
                 */
                
                
                Spacer()
                
            }.frame(width: geometry.size.width, height: geometry.size.height)
                .disabled(self.action==4)
                .onAppear{
                    dateFormatter.dateFormat = "yyyy/MM/dd"
                    birthDate = dateFormatter.date(from: "2030/01/01")!
                }
        }
    }
    
    func register(){
        if birthDate > Date(){
            changeMessage(message: "Please select your birthday")
            self.action = 2
        }
        else if username.count == 0{
            changeMessage(message: "Please write your email")
            self.action = 2
        }
        else if username.count<5{
            changeMessage(message: "The email is too short")
            self.action = 2
        }
        else if password.count==0{
            changeMessage(message: "Please write your password")
            self.action = 2
        }
        else if password.count<8{
            changeMessage(message: "The password is too short")
            self.action = 2
        }
        else if password != password2{
            changeMessage(message: "Passwords don't match")
            self.action = 2
        }
        else if name.count<3{
            changeMessage(message: "Enter a longer name")
            self.action = 2
        }        else if lastName.count<3{
            changeMessage(message: "Enter a longer name")
            self.action = 2
        }
        else if !networkState.status{
            changeMessage(message: "No internet connection")
            self.action = 2
        }
        else{
            let lastNameRange = lastName.rangeOfCharacter(from: decimalCharacters)
            let nameRange = name.rangeOfCharacter(from: decimalCharacters)
            if lastNameRange != nil {
                changeMessage(message: "Last name can't contain numbers")
                self.action = 2
            }
            else if nameRange != nil {
                changeMessage(message: "Name can't contain numbers")
                self.action = 2
            }
            else{
                Auth.auth().createUser(withEmail: self.username, password: self.password) { (result, error) in
                    if error != nil {
                        if let errCode = AuthErrorCode(rawValue: error!._code){
                            switch errCode {
                            case .invalidEmail:
                                changeMessage(message: "Invalid email")
                            case .weakPassword:
                                changeMessage(message: "Weak password")
                            case .emailAlreadyInUse:
                                changeMessage(message: "Email already in use")
                            default:
                                changeMessage(message: error?.localizedDescription ?? "Better luck next time :(")
                            }
                        }
                        self.action = 2
                    } else {
                        //user.questions = true
                        user.updateFullIni()
                        user.name = self.name
                        user.lastName = self.lastName
                        user.birthdate = self.birthDate
                        createUser()
                        self.action = 1
                    }
                }
            }
        }
    }
}


