//
//  LogIn.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/10/22.
//

import SwiftUI
import Firebase

struct LogIn: View {
    
    @State private var offsets = (top: CGFloat.zero, bottom: CGFloat.zero)
    @State private var offset: CGFloat = .zero
    @State private var lastOffset: CGFloat = .zero
    @State private var showView = true
    @State var currentSize: CGSize = CGSize(width: 0, height: 0)
    
    func reset(){
        showView = true
    }
    
    var body: some View {
        GeometryReader { geometry in
            ZStack{
                    Image("fondoLogIn")
                        .resizable()
                        .ignoresSafeArea()
                    VStack {
                        if showView{
                            Image("logo").resizable().scaledToFit()
                            Spacer()
                        }
                    }.frame(width: currentSize.width, height: currentSize.height)
                    VStack (spacing: 15) {
                        RoundedRectangle(cornerRadius: 5)
                            .foregroundColor(.gray)
                            .frame(width: 150, height: 8)
                        if showView {
                            Text("Slide up to start!")
                                .font(.title)
                                .fontWeight(.heavy)
                                .foregroundColor(Color("Color2"))
                        }
                        else{
                            Main()
                        }
                        Spacer()
                    }
                    .padding()
                    .frame(width: currentSize.width, height: currentSize.height)
                    .background(Color("Color1"))
                    .clipShape(RoundedRectangle(cornerRadius: min(self.offset, 20) ))
                    .animation(.interactiveSpring(), value: offset)
                    .onChange(of: currentSize, perform: {newValue in
                        self.offsets = (
                            top: newValue.height * 0.1,
                            bottom: newValue.height * 7 / 8
                        )
                        self.offset = self.offsets.bottom
                        self.lastOffset = self.offset
                    })
                    .offset(y: self.offset)
                    .gesture(DragGesture(minimumDistance: 5)
                        .onChanged { v in
                            let newOffset = self.lastOffset + v.translation.height
                            if (newOffset > self.offsets.top && newOffset < self.offsets.bottom) {
                                self.offset = newOffset
                            }
                        }
                        .onEnded{ v in
                            if (self.lastOffset == self.offsets.top && v.translation.height > 0) {
                                if (v.translation.height > currentSize.height / 4) {
                                    self.offset = self.offsets.bottom
                                    self.showView = true
                                }
                                else{
                                    self.offset = self.offsets.top
                                    self.showView = false
                                }
                            } else if (self.lastOffset == self.offsets.bottom && v.translation.height < 0) {
                                if (abs(v.translation.height) > currentSize.height / 4) {
                                    self.offset = self.offsets.top
                                    self.showView = false
                                }
                                else{
                                    self.offset = self.offsets.bottom
                                    self.showView = true
                                }
                            }
                            self.lastOffset = self.offset
                        }
                    )
            }
            .edgesIgnoringSafeArea(.all)
            .onChange(of: geometry.size) { newSize in
                self.currentSize = newSize
            }
            .coordinateSpace(name: "VStack")
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
    }
}


struct Main: View {
    @State var username: Int = 0
    @EnvironmentObject var user: User
    @EnvironmentObject var fm: FirestoreManager
    
    func reset(){
        username = 0
    }
    
    func updateUser(){
        guard let currentUser = Auth.auth().currentUser else { return }
        user.uID = currentUser.uid
        user.email = currentUser.email ?? ""
    }
    func createUser(){
        guard let currentUser = Auth.auth().currentUser else { return }
        user.uID = currentUser.uid
        user.email = currentUser.email ?? ""
        fm.setUser(user: user.uID, userData: user)
        fm.setProfile(user: user.uID, userData: user)
        fm.setLikes(user: user.uID)
        fm.setRecommended(user: user.uID)
        reset()
        Auth.auth().currentUser?.sendEmailVerification { error in
            //print(error)
        }
    }
    func onRegister() {
        username = 2
    }
    func onHome() {
        username = 0
    }
    func onForgot() {
        username = 1
    }
    var body: some View {
        
        if (username==0){
            LogInForm(onRegister:self.onRegister,onForgot:self.onForgot,updateUser: self.updateUser)
        }
        else if (username==1){
            Forgot(onHome:self.onHome)
        }
        else if (username==2){
            RegisterForm(onHome:self.onHome,createUser: self.createUser)
        }
    }
    
    
}

enum Fields {
    case field1
    case field2
    case field3
    case field4
    case field5
    case field6
}
