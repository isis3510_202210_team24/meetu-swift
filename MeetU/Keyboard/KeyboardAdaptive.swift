//
//  KeyboardAdaptive.swift
//  MeetU
//
//  Created by Andrés Martínez on 4/3/22 adapted from https://github.com/V8tr/KeyboardAvoidanceSwiftUI/blob/master/KeyboardAvoidanceSwiftUI/KeyboardAdaptive.swift
//

import SwiftUI
import Combine

struct KeyboardAdaptive: ViewModifier {
    @State private var keyboardHeight: CGFloat = 0

    func body(content: Content) -> some View {
        content
            .padding(.bottom, keyboardHeight)
            .onReceive(Publishers.keyboardHeight) { self.keyboardHeight = $0 }
    }
}

extension View {
    func keyboardAdaptive() -> some View {
        ModifiedContent(content: self, modifier: KeyboardAdaptive())
    }
}
