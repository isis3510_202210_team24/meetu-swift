//
//  ActivityDetail.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/9/22.
//

import SwiftUI
import MapKit

struct ActivityDetail: View {
    
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @GestureState private var dragOffset = CGSize.zero
    @EnvironmentObject var fm: FirestoreManager
    @EnvironmentObject private var networkState: NetworkMonitor
    @EnvironmentObject private var user: User
    @State var noConnection = false
    var activity: Activity
    @State var region: MKCoordinateRegion
    var place: IdentifiablePlace
    let dateFormatter = DateFormatter()
    var offsetView: CGFloat = -210
    var url: URL?
    let placeholder = "https://uning.es/wp-content/uploads/2016/08/ef3-placeholder-image.jpg"
    
    init(activity: Activity, region: MKCoordinateRegion, place: IdentifiablePlace) {
        self.activity = activity
        _region = State(initialValue: region)
        self.place = place
        dateFormatter.dateFormat = "yyyy/MM/dd HH:mm"
        self.url = URL(string: "maps://?saddr=&daddr=\(activity.latitude),\(activity.longitude)")
    }
    
    
    var body: some View {
        ZStack{
            Color("Color1")
                .ignoresSafeArea()
            VStack{
                VStack{
                    //Map(coordinateRegion: $region)
                    Map(coordinateRegion: $region,
                        interactionModes: [],
                        annotationItems: [place])
                    { place in
                        MapMarker(coordinate: place.location,
                                  tint: Color("Color3"))
                    }
                    .frame(height: 250)
                    .edgesIgnoringSafeArea(.top)
                    VStack{
                        AsyncImage(
                            url: URL(string: activity.imageUrl) ?? URL(string: placeholder)!,
                            placeholder: { Image("placeholder") },
                            image: { Image(uiImage: $0).resizable()
                            }
                        )
                        .scaledToFill()
                        .transition(.slide)
                        .frame(width: 120, height: 120)
                        .clipped()
                        .background()
                        .clipShape(Circle())
                        .overlay(Circle().stroke(Color.white, lineWidth: 4))
                        .shadow(radius: 10)
                        Text(activity.name)
                            .fontWeight(.heavy)
                            .font(.largeTitle)
                            .foregroundColor(Color("Color2"))
                            .padding(.bottom, 10)
                        Text(activity.description)
                            .fontWeight(.regular)
                            .font(.body)
                            .padding(.bottom, 20)
                        Group{
                            HStack{
                                Text("Date")
                                    .fontWeight(.heavy)
                                    .font(.title3)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.leading, 30)
                                Spacer()
                                Text(dateFormatter.string(from: activity.date))
                                    .fontWeight(.regular)
                                    .font(.body)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.trailing, 30)
                            }
                            .padding(.bottom,10)
                            HStack{
                                Text("Host")
                                    .fontWeight(.heavy)
                                    .font(.title3)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.leading, 30)
                                Spacer()
                                Text(activity.ownerName)
                                    .fontWeight(.regular)
                                    .font(.body)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.trailing, 30)
                            }
                            .padding(.bottom,10)
                            HStack{
                                Text("Address")
                                    .fontWeight(.heavy)
                                    .font(.title3)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.leading, 30)
                                Spacer()
                                Text(activity.address)
                                    .fontWeight(.regular)
                                    .font(.body)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.trailing, 30)
                            }
                            .padding(.bottom,10)
                            HStack{
                                Text("Contact")
                                    .fontWeight(.heavy)
                                    .font(.title3)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.leading, 30)
                                Spacer()
                                Text(activity.email)
                                    .fontWeight(.regular)
                                    .font(.body)
                                    .foregroundColor(Color("Color2"))
                                    .padding(.trailing, 30)
                            }
                            .padding(.bottom,10)
                        }
                    }
                    .offset(y: offsetView)
                    Spacer()
                }
                .padding(.bottom, offsetView)
                Spacer()
                Button("Directions"){
                    if UIApplication.shared.canOpenURL(url!) {
                        UIApplication.shared.open(url!, options: [:], completionHandler: nil)
                    }
                }
                .buttonStyle(NiceButtonStyle())
                if activity.idOwner == user.uID{
                    Button("Delete"){
                        if networkState.status{
                            noConnection = false
                            fm.deleteActivity(id: activity.id!)
                            self.mode.wrappedValue.dismiss()
                        }
                        else{
                            noConnection = true
                        }
                    }
                    .buttonStyle(NiceButtonStyle())
                    .padding(.top,7)
                }
                if noConnection {
                    Text("No connection, try again")
                        .fontWeight(.heavy)
                        .foregroundColor(Color("Color3"))
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarBackButtonHidden(true)
        .navigationBarItems(leading: Button(action : {
            self.mode.wrappedValue.dismiss()
        }){
            Text("Back")
                .padding()
                .frame(width: 70, height: 35)
                .background(Color("Color3"))
                .clipShape(Capsule())
                .foregroundColor(Color.white)
                .shadow(radius: 5)
        })
        .gesture(DragGesture().updating($dragOffset, body: { (value, state, transaction) in
            if(value.startLocation.x < 20 && value.translation.width > 100) {
                self.mode.wrappedValue.dismiss()
            }
        }))
        
    }
}

struct IdentifiablePlace: Identifiable {
    let id: UUID
    let location: CLLocationCoordinate2D
    init(id: UUID = UUID(), lat: Double, long: Double) {
        self.id = id
        self.location = CLLocationCoordinate2D(
            latitude: lat,
            longitude: long)
    }
}

/*
 
 @EnvironmentObject private var locationManager: LocationManager
 @EnvironmentObject private var fm: FirestoreManager
 @EnvironmentObject private var networkMonitor: NetworkMonitor
 
 switch locationManager.authorizationStatus {
 case .notDetermined:
 Map(coordinateRegion: $region)
 .frame(height: 250)
 .edgesIgnoringSafeArea(.top)
 .onAppear{
 locationManager.requestPermission()
 }
 case .denied, .restricted:
 Map(coordinateRegion: $region)
 .frame(height: 250)
 .edgesIgnoringSafeArea(.top)
 case .authorizedAlways, .authorizedWhenInUse:
 Map(coordinateRegion: $region, interactionModes: [], showsUserLocation: true, userTrackingMode: .constant(.follow))
 .frame(height: 250)
 .edgesIgnoringSafeArea(.top)
 default:
 Map(coordinateRegion: $region)
 .frame(height: 250)
 .edgesIgnoringSafeArea(.top)
 }
 Group{
 HStack{
 Text("Pais")
 .fontWeight(.heavy)
 .font(.title3)
 .foregroundColor(Color("Color3"))
 .padding(.leading, 30)
 
 Spacer()
 Text(locationManager.currentPlacemark?.country ?? "")
 .fontWeight(.regular)
 .font(.caption)
 .foregroundColor(Color("Color3"))
 .padding(.trailing, 30)
 }
 .padding(.bottom, 6)
 HStack{
 Text("Ciudad")
 .fontWeight(.heavy)
 .font(.title3)
 .foregroundColor(Color("Color3"))
 .padding(.leading, 30)
 Spacer()
 Text(locationManager.currentPlacemark?.administrativeArea ?? "")
 .fontWeight(.regular)
 .font(.caption)
 .foregroundColor(Color("Color3"))
 .padding(.trailing, 30)
 }
 .padding(.bottom, 6)
 HStack{
 Text("Thoroughfare")
 .fontWeight(.heavy)
 .font(.title3)
 .foregroundColor(Color("Color3"))
 .padding(.leading, 30)
 Spacer()
 Text(locationManager.currentPlacemark?.thoroughfare ?? "")
 .fontWeight(.regular)
 .font(.caption)
 .foregroundColor(Color("Color3"))
 .padding(.trailing, 30)
 }
 .padding(.bottom, 6)
 HStack{
 Text("subThoroughfare")
 .fontWeight(.heavy)
 .font(.title3)
 .foregroundColor(Color("Color3"))
 .padding(.leading, 30)
 Spacer()
 Text(locationManager.currentPlacemark?.subThoroughfare ?? "")
 .fontWeight(.regular)
 .font(.caption)
 .foregroundColor(Color("Color3"))
 .padding(.trailing, 30)
 }
 .padding(.bottom, 6)
 HStack{
 Text("Postal code")
 .fontWeight(.heavy)
 .font(.title3)
 .foregroundColor(Color("Color3"))
 .padding(.leading, 30)
 Spacer()
 Text(locationManager.currentPlacemark?.postalCode ?? "")
 .fontWeight(.regular)
 .font(.caption)
 .foregroundColor(Color("Color3"))
 .padding(.trailing, 30)
 }
 .padding(.bottom, 6)
 HStack{
 Text("Locality")
 .fontWeight(.heavy)
 .font(.title3)
 .foregroundColor(Color("Color3"))
 .padding(.leading, 30)
 Spacer()
 Text(locationManager.currentPlacemark?.locality ?? "")
 .fontWeight(.regular)
 .font(.caption)
 .foregroundColor(Color("Color3"))
 .padding(.trailing, 30)
 }
 }
 
 
 
 
 Spacer()
 Button("Return"){
 
 }
 Button("new activity"){
 fm.addActivity(networkState: networkMonitor.status, activity: Activity(name: "Gym",
 description: "Ir al gym",
 date: Date(),
 address: "Aquí",
 city: "Bogotá",
 state: "Bogotá",
 country: "Colombia",
 latitude: 0,
 longitude: 0,
 imageUrl: "empty",
 phone: "123456789",
 email: "gym@email.com",
 idOwner: "hbjhdbvjhf",
 ownerName: "Mario"
 )
 )
 }
 Button("fetch activities"){
 DispatchQueue.global(qos: .userInitiated).async {
 Task{
 await fm.fetchActivities(networkState: networkMonitor.status)
 }
 }
 }
 Button("print activities"){
 print("activities")
 print(fm.activities)
 }
 Spacer()
 */
