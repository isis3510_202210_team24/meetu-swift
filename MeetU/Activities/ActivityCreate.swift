//
//  ActivityCreate.swift
//  MeetU
//
//  Created by Danna Mayorga on 25/05/22.
//

import SwiftUI
import MapKit
import Photos
import CoreLocation
import Combine

struct ActivityCreate: View{
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    @EnvironmentObject var fm: FirestoreManager
    @EnvironmentObject private var user: User
    @EnvironmentObject private var networkState: NetworkMonitor
    @EnvironmentObject private var locationManager: LocationManager
    @State var activityName: String = ""
    @State var phoneNumber: String = ""
    @State var email: String = ""
    @State var description: String = ""
    @State var eventDate: Date = Date(timeIntervalSince1970: 0)
    @State var categories:[String] = ["Not selected","Rest", "Adventure", "Animals", "Videogames","Exercise", "Eating", "Cooking", "Art", "Music", "Dance"]
    @State var categoryPicked: String = ""
    @State var placeLat: Double = 0.0
    @State var placeLong: Double = 0.0
    //@State var region: MKCoordinateRegion
    @State var place = [IdentifiablePlace]()
    @State var address: String = ""
    @State var checkFields = false
    @State var showCaptureImageView: Bool = false
    @State var source: Bool = false
    @State var image: Image? = nil
    @State var rawImage: UIImage? = nil
    @State var showUserLoc = false
    @State var offAlert = false
    var dateClosedRange: ClosedRange<Date> {
        let min = Calendar.current.date(byAdding: .year, value: 0, to: Date())!
        let max = Calendar.current.date(byAdding: .year, value: 2, to: Date())!
        return min...max
    }
    
    func checkAllFields() -> Bool{
        
        if(!activityName.isEmpty && !phoneNumber.isEmpty && !email.isEmpty && !description.isEmpty && !place.isEmpty && !categoryPicked.isEmpty){
            return true
        }
        
        return false
    }
    func trigger(){
        self.showCaptureImageView.toggle()
    }
    
    func permissionLibrary(){
        PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
            switch status {
            case .notDetermined:
                return
            case .restricted:
                return
            case .denied:
                return
            case .authorized:
                self.trigger()
            case .limited:
                self.trigger()
            @unknown default:
                fatalError()
            }
        }
    }
    
    func cameraRoll(){
        self.source = false
        self.permissionLibrary()
    }
    
    @State private var region = MKCoordinateRegion(
        center: CLLocationCoordinate2D(latitude: 4.5921,
                                       longitude: -74.1240),
        latitudinalMeters: 750,
        longitudinalMeters: 750
    )
    
    func getAddressFromLatLon(latitude: Double, longitude: Double) {
        
        let ceo: CLGeocoder = CLGeocoder()
        let loc: CLLocation = CLLocation(latitude: latitude, longitude: longitude)
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
                                    {(placemarks, error) in
            if (error != nil)
            {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            }
            let pm = placemarks! as [CLPlacemark]
            
            if pm.count > 0 {
                let pm = placemarks![0]
                let thoroughfare = (pm.thoroughfare ?? "")
                let subThoroughfare = (pm.subThoroughfare ?? "")
                DispatchQueue.main.async {
                    address = thoroughfare + " # " + subThoroughfare
                }
                
            }
        })
    }
    
    
    
    var body: some View {
        
        if (showCaptureImageView) {
            CaptureImageView(isShown: $showCaptureImageView, image: $image, rawImage: $rawImage, source: $source)
                .ignoresSafeArea()
                .navigationBarTitle("")
                .navigationBarHidden(true)
        }
        else{
            GeometryReader { geometry in
                ScrollView{
                    VStack{
                        Text("Create Activity")
                            .font(.system(size: 27, weight: .bold))
                            .foregroundColor(Color("Color3"))
                            .padding([.horizontal,.bottom])
                        
                        
                    }.frame(width: geometry.size.width, height: geometry.size.height * 0.06)
                        .background(Color("Color1"))
                        .position(x:geometry.size.width * 0.499, y: geometry.size.height * 0.01)
                    
                    VStack(alignment: .leading){
                        Text("Select Image")
                            .font(.headline)
                            .colorInvert()
                            .colorMultiply(Color("Color2"))
                            .padding(.horizontal)
                            .padding(.top,40)
                        VStack{
                            if(image != nil){
                                image?.resizable()
                                    .frame(width:geometry.size.width * 0.90,height: geometry.size.height * 0.4)
                                    .clipped()
                                    .cornerRadius(23)
                                
                            }
                            else{
                                Image("placeholder").scaledToFill()
                                    .transition(.slide)
                                    .frame(width:geometry.size.width * 0.90,height: geometry.size.height * 0.4)
                                    .clipped()
                                    .cornerRadius(23)
                            }
                        }.padding(.horizontal)
                        Button("Camera Roll"){
                            cameraRoll()
                        }
                        .padding(.top, geometry.size.width * 0.14)
                        .padding(.bottom, -(geometry.size.height * 0.03))
                        .buttonStyle(NiceButtonStyle())
                        .position(x:geometry.size.width * 0.48)
                        
                        Group{
                            Group{
                                Text("Name")
                                    .font(.headline)
                                    .colorInvert()
                                    .colorMultiply(Color("Color2"))
                                    .padding(.horizontal)
                                TextField("Event name", text: $activityName)
                                    .frame(width: geometry.size.width * 0.9)
                                    .textFieldStyle(ActivityTextFieldStyle())
                                    .padding(.horizontal)
                                    .padding(.bottom, 15)
                                if(checkFields && activityName.isEmpty){
                                    Text("Insert the event name!")
                                        .font(.headline)
                                        .colorInvert()
                                        .colorMultiply(Color("Color3"))
                                        .padding([.horizontal, .bottom])
                                        .padding(.top, -10)
                                }
                            }
                            Group{
                                Text("Phone number")
                                    .font(.headline)
                                    .colorInvert()
                                    .colorMultiply(Color("Color2"))
                                    .padding(.horizontal)
                                TextField("Contact phone number", text: $phoneNumber)
                                    .keyboardType(.numberPad)
                                    .onReceive(Just(phoneNumber)) { newValue in
                                        let filtered = newValue.filter { "0123456789".contains($0) }
                                        if filtered != newValue {
                                            self.phoneNumber = filtered
                                        }
                                }
                                    .frame(width: geometry.size.width * 0.9)
                                    .textFieldStyle(ActivityTextFieldStyle())
                                    .padding(.horizontal)
                                    .padding(.bottom, 15)
                                if(checkFields && phoneNumber.isEmpty){
                                    Text("Insert a phone number!")
                                        .font(.headline)
                                        .colorInvert()
                                        .colorMultiply(Color("Color3"))
                                        .padding([.horizontal, .bottom])
                                        .padding(.top, -10)
                                }
                            }
                            Group {
                                Text("Email")
                                    .font(.headline)
                                    .colorInvert()
                                    .colorMultiply(Color("Color2"))
                                    .padding(.horizontal)
                                TextField("Contact email", text: $email)
                                    .frame(width: geometry.size.width * 0.9)
                                    .textFieldStyle(ActivityTextFieldStyle())
                                    .padding(.horizontal)
                                    .padding(.bottom, 15)
                                if(checkFields && email.isEmpty){
                                    Text("Insert an email!")
                                        .font(.headline)
                                        .colorInvert()
                                        .colorMultiply(Color("Color3"))
                                        .padding([.horizontal, .bottom])
                                        .padding(.top, -10)
                                }
                            }
                            Group{
                                Text("Description")
                                    .font(.headline)
                                    .colorInvert()
                                    .colorMultiply(Color("Color2"))
                                    .padding(.horizontal)
                                TextField("Event description", text: $description)
                                    .frame(width: geometry.size.width * 0.9)
                                    .textFieldStyle(ActivityTextFieldStyle())
                                    .padding(.horizontal)
                                    .padding(.bottom, 15)
                                if(checkFields && description.isEmpty){
                                    Text("Insert a description!")
                                        .font(.headline)
                                        .colorInvert()
                                        .colorMultiply(Color("Color3"))
                                        .padding([.horizontal, .bottom])
                                        .padding(.top, -10)
                                }
                            }
                        }
                        
                        DatePicker("Event Date", selection: $eventDate, in: dateClosedRange, displayedComponents: [.date, .hourAndMinute])
                            .font(.headline)
                            .frame(width: geometry.size.width * 0.9, height: nil)
                            .colorInvert()
                            .colorMultiply(Color("Color2"))
                            .submitLabel(.next)
                        //.focused($focus, equals: .field5)
                            .padding(.bottom, 25)
                            .padding(.top, 8)
                            .padding(.horizontal)
                        if(checkFields && eventDate == Date(timeIntervalSince1970: 0)){
                            Text("Select a date!")
                                .font(.headline)
                                .colorInvert()
                                .colorMultiply(Color("Color3"))
                                .padding([.horizontal, .bottom])
                                .padding(.top, -10)
                        }
                        
                        HStack{
                            Text("Category").padding(.horizontal)
                                .font(.headline)
                                .colorInvert()
                                .colorMultiply(Color("Color2"))
                                .padding(.bottom)
                            Picker("Category",selection: $categoryPicked){
                                ForEach(categories,id: \.self) { category in
                                    Text(category)
                                        .tag(String?.some(category))
                                }
                            }
                            .frame(width: 130)
                            .colorMultiply(Color("Color2"))
                            .background(Color(red: 211/255, green: 206/255, blue: 194/250))
                            .cornerRadius(9)
                            .padding(.leading, geometry.size.width * 0.326)
                            .padding(.bottom)
                        }
                        if(checkFields && categoryPicked == ""){
                            Text("Select a category!")
                                .font(.headline)
                                .colorInvert()
                                .colorMultiply(Color("Color3"))
                                .padding([.horizontal, .bottom])
                                .padding(.top, -10)
                        }
                        VStack(alignment:.leading){
                            Text("Select location").padding(.horizontal)
                                .font(.headline)
                                .colorInvert()
                                .colorMultiply(Color("Color2"))
                            ZStack{
                                Map(coordinateRegion: $region, showsUserLocation: showUserLoc,
                                    annotationItems: place)
                                { place in
                                    MapMarker(coordinate: place.location,
                                              tint: Color("Color3"))
                                }
                                .cornerRadius(20)
                                .padding([.bottom,.horizontal])
                                .frame(width: geometry.size.width, height: 300)
                                Circle()
                                    .fill(.blue)
                                    .opacity(0.3)
                                    .frame(width: 32, height: 32)
                            }
                            if(checkFields && place.isEmpty){
                                Text("Select a place on the map!")
                                    .font(.headline)
                                    .colorInvert()
                                    .colorMultiply(Color("Color3"))
                                    .padding([.horizontal, .bottom])
                                    .padding(.top, -10)
                            }
                            Button(action: {
                                placeLat = region.center.latitude
                                placeLong = region.center.longitude
                                getAddressFromLatLon(latitude: placeLat, longitude: placeLong)
                                
                                print(String(placeLat) + " : " + String(placeLong))
                                place = [IdentifiablePlace(lat: placeLat, long: placeLong)]
                            },label: {
                                Text("Select Place")
                                    .frame(width: 120, height: 40)
                                    .background(Color("Color3")).cornerRadius(15)
                                    .foregroundColor(.white)
                                    .padding(.leading, geometry.size.width * 0.65)
                            })
                            Text("Address")
                                .font(.headline)
                                .colorInvert()
                                .colorMultiply(Color("Color2"))
                                .padding(.horizontal)
                            TextField("Event Address", text: $address)
                                .frame(width: geometry.size.width * 0.9)
                                .textFieldStyle(ActivityTextFieldStyle())
                                .padding(.horizontal)
                                .padding(.bottom, 15)
                        }.padding([.top,.bottom])
                        
                        VStack(alignment: .center){
                            Button(action: {
                                checkFields = true
                                print(categoryPicked)
                                if(networkState.status){
                                    if(checkAllFields())
                                    {
                                        let loc: CLLocation = CLLocation(latitude: placeLat, longitude: placeLong)
                                        let geocoder = CLGeocoder()
                                        geocoder.reverseGeocodeLocation(loc) { (placemarks, error) in
                                            let pm = placemarks! as [CLPlacemark]
                                            
                                            if pm.count > 0 {
                                                let pm = placemarks![0]
                                                let city = (pm.locality ?? "")
                                                let country = (pm.country ?? "")
                                                let state = (pm.administrativeArea ?? "")
                                                let activity = Activity(name: activityName, description: description, date: eventDate, address: address, city: city, state: state, country: country, latitude: placeLat, longitude: placeLong, imageUrl: "", phone: phoneNumber, email: email, idOwner: user.uID, ownerName: fm.userProfile.name, type: categoryPicked)
                                                let id = fm.addActivity(networkState: networkState.status, activity: activity)
                                                if let rawImage = rawImage {
                                                    DispatchQueue.global(qos: .userInitiated).async {
                                                        fm.setImageActivity(activity: id, image: rawImage.jpegData(compressionQuality: 0.2)!)
                                                    }
                                                }
                                            }
                                        }
                                        
                                        fm.updateActivityStats(userId: user.uID, dateCreated: Date.now)
                                        self.mode.wrappedValue.dismiss()
                                        
                                    }
                                }
                                else{
                                    offAlert = true
                                }
                            }
                                   ,label: {Text("Create")}
                            )
                            .buttonStyle(NiceButtonStyle())
                            if(checkFields && !checkAllFields()){
                                Text("Some fields are empty!")
                                    .font(.headline)
                                    .colorInvert()
                                    .colorMultiply(Color("Color3"))
                                    .padding([.horizontal, .bottom])
                                    .padding(.top, -5)
                            }
                        }.padding(.leading, geometry.size.width * 0.25)
                            .padding(.bottom)
                        
                    }.padding(.top, -(geometry.size.width * 0.06))
                        .background(Color("Color1"))
                }.background(Color("Color1"))
            }    .navigationBarTitle("")
                .navigationBarBackButtonHidden(true)
                .navigationBarItems(leading: Button(action : {
                    self.mode.wrappedValue.dismiss()
                }){
                    Text("Back")
                        .padding()
                        .frame(width: 70, height: 35)
                        .background(Color("Color3"))
                        .clipShape(Capsule())
                        .foregroundColor(Color.white)
                        .shadow(radius: 5)
                })
                .onAppear(){
                    if locationManager.authorizationStatus == .notDetermined{
                        locationManager.requestPermission()
                    }
                    else if locationManager.authorizationStatus == .authorizedAlways || locationManager.authorizationStatus == .authorizedWhenInUse{
                        showUserLoc = true
                        region = MKCoordinateRegion(
                            center: CLLocationCoordinate2D(latitude: locationManager.lastSeenLocation?.coordinate.latitude ?? 4.5921,
                                                           longitude: locationManager.lastSeenLocation?.coordinate.longitude ?? -74.1240),
                            latitudinalMeters: 750,
                            longitudinalMeters: 750
                        )
                        
                    }
                }
                .alert(isPresented: $offAlert){
                    Alert(title: Text("You can't create a new activity"),
                          message: Text("Try again when you are back online"),
                          dismissButton: .default(Text("Fine..")))
                }
        }
        
        
    }
}

struct ActivityTextFieldStyle: TextFieldStyle {
    func _body(configuration: TextField<_Label>) -> some View {
        configuration
            .multilineTextAlignment(.leading)
            .foregroundColor((Color("Color1")))
            .background(
                RoundedRectangle(cornerRadius: 10)
                    .fill(Color("Color2"))
                    .frame(height: 40)
            )
            
            //.textFieldStyle(.roundedBorder)
            //.textFieldStyle(RoundedBorderTextFieldStyle())
    }
}
