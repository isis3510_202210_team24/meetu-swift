//
//  ActivityList.swift
//  MeetU
//
//  Created by Mario Andrade on 12/05/22.
//

import SwiftUI
import MapKit
import CoreLocation

struct ActivityList: View{
    @EnvironmentObject var fm: FirestoreManager
    @EnvironmentObject private var networkState: NetworkMonitor
    @EnvironmentObject private var locationManager: LocationManager
    @EnvironmentObject private var user: User
    let placeholder = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/1200px-Placeholder_no_text.svg.png"
    
    @State var gridLayout: [GridItem] = [ GridItem()]
    @State var activities: [String ] = ["Sports", "Study", "Party", "Relax", "Adventure", "Animals", "Videogames","Exercise", "Eating", "Cooking", "Art", "Music", "Dance"]
    
    var body: some View {
        GeometryReader { geometry in
            VStack{
                Text("Activities")
                    .foregroundColor(Color("Color3"))
                    .font(.title)
                    .fontWeight(.heavy)
                    .padding()
                ScrollView(.vertical){
                    PullToRefresh(coordinateSpaceName: "pullToRefresh") {
                        Task{
                            await fm.fetchActivities(networkState: networkState.status)
                        }
                    }
                    LazyVGrid(columns: gridLayout, spacing: 20){
                        let generalActivities = Array(fm.activities.enumerated())
                        
                        // Close to you activities
                        switch locationManager.authorizationStatus {
                        case .notDetermined:
                            HStack{
                                Text("Close to you")
                                    .padding(.leading, 10)
                                    .font(.system(size: 23, weight: .bold))
                                Spacer()
                            }.onAppear{
                                locationManager.requestPermission()
                            }
                        case .denied, .restricted:
                            EmptyView()
                        case .authorizedAlways, .authorizedWhenInUse:
                            
                            
                            let loc = locationManager.lastSeenLocation
                            if let loc = loc {
                                // Close to you
                                HStack{
                                    Text("Close to you")
                                        .padding(.leading, 10)
                                        .font(.system(size: 23, weight: .bold))
                                    Spacer()
                                }
                                let sortedActivities = generalActivities.sorted{ elem1, elem2 in
                                    let coordinate1 = CLLocation(latitude: elem1.element.latitude, longitude: elem1.element.longitude)
                                    let coordinate2 = CLLocation(latitude: elem2.element.latitude, longitude: elem2.element.longitude)
                                    let distanceInMeters1 = loc.distance(from: coordinate1)
                                    let distanceInMeters2 = loc.distance(from: coordinate2)
                                    return distanceInMeters1<distanceInMeters2
                                }
                                ScrollView(.horizontal){
                                    LazyHGrid(rows: gridLayout, alignment: .center, spacing: 15) {
                                        
                                        ForEach(sortedActivities.prefix(20), id: \.offset) { index, activityI in
                                            let place = IdentifiablePlace(lat: activityI.latitude, long: activityI.longitude)
                                            let region = MKCoordinateRegion(
                                                center: CLLocationCoordinate2D( latitude: activityI.latitude-0.005, longitude: activityI.longitude),
                                                span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                                            )
                                            NavigationLink(destination: ActivityDetail(activity: activityI, region: region, place: place), label:{
                                                
                                                ActivityCard(height: geometry.size.height, width: geometry.size.width, name: activityI.name, city: activityI.city, URLpic: activityI.imageUrl)
                                                    .frame(minHeight: 0, maxHeight: geometry.size.height * 0.9)
                                                    .frame(width: geometry.size.width * 0.75)
                                                    .padding([.bottom], geometry.size.width * 0.03)
                                                    .cornerRadius(15)
                                                    .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                                                
                                            } )
                                        }
                                        
                                    }
                                }.padding([.leading], geometry.size.width * 0.03)
                                    .frame(height: geometry.size.height * 0.18)
                                
                                // In your city
                                /*
                                if let city = locationManager.currentPlacemark?.locality{
                                    let filteredActivitiesL = sortedActivities.filter{ activity in
                                        return activity.element.city == city
                                    }
                                    
                                    if filteredActivitiesL.count > 0{
                                        HStack{
                                            Text("In your city")
                                                .padding(.leading, 10)
                                                .font(.system(size: 23, weight: .bold))
                                            Spacer()
                                        }
                                    
                                        
                                        ScrollView(.horizontal){
                                            LazyHGrid(rows: gridLayout, alignment: .center, spacing: 15) {
                                                
                                                ForEach(filteredActivitiesL, id: \.offset) { index, activityI in
                                                    let place = IdentifiablePlace(lat: activityI.latitude, long: activityI.longitude)
                                                    let region = MKCoordinateRegion(
                                                        center: CLLocationCoordinate2D( latitude: activityI.latitude-0.005, longitude: activityI.longitude),
                                                        span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                                                    )
                                                    NavigationLink(destination: ActivityDetail(activity: activityI, region: region, place: place), label:{
                                                        
                                                        ActivityCard(height: geometry.size.height, width: geometry.size.width, name: activityI.name, city: activityI.city, URLpic: activityI.imageUrl)
                                                            .frame(minHeight: 0, maxHeight: geometry.size.height * 0.9)
                                                            .frame(width: geometry.size.width * 0.75)
                                                            .padding([.bottom], geometry.size.width * 0.03)
                                                            .cornerRadius(15)
                                                            .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                                                        
                                                    } )
                                                }
                                                
                                            }
                                        }.padding([.leading], geometry.size.width * 0.03)
                                            .frame(height: geometry.size.height * 0.18)
                                    }
                                    
                                }
                                */
                            }
                        default:
                            EmptyView()
                        }
                        
                        // Your activities
                        HStack{
                            Text("Your activities")
                                .padding(.leading, 10)
                                .font(.system(size: 23, weight: .bold))
                            Spacer()
                        }
                        let filteredActivities = generalActivities.filter{ activity in
                            return activity.element.idOwner == user.uID
                        }
                        ScrollView(.horizontal){
                            LazyHGrid(rows: gridLayout, alignment: .center, spacing: 15) {
                                ForEach(filteredActivities, id: \.offset) { index, activityI in
                                    let place = IdentifiablePlace(lat: activityI.latitude, long: activityI.longitude)
                                    let region = MKCoordinateRegion(
                                        center: CLLocationCoordinate2D( latitude: activityI.latitude-0.005, longitude: activityI.longitude),
                                        span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                                    )
                                    NavigationLink(destination: ActivityDetail(activity: activityI, region: region, place: place), label:{
                                        
                                        ActivityCard(height: geometry.size.height, width: geometry.size.width, name: activityI.name, city: activityI.city, URLpic: activityI.imageUrl)
                                            .frame(minHeight: 0, maxHeight: geometry.size.height * 0.9)
                                            .frame(width: geometry.size.width * 0.75)
                                            .padding([.bottom], geometry.size.width * 0.03)
                                            .cornerRadius(15)
                                            .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                                        
                                    } )
                                }
                                
                                NavigationLink(destination: ActivityCreate(), label: {
                                    Image("plusCircular")
                                        .resizable()
                                        .scaledToFit()
                                        .frame(minHeight: 0, maxHeight: geometry.size.height * 0.9)
                                        .frame(width: geometry.size.width * 0.75)
                                        .padding([.bottom,.top], geometry.size.width * 0.05)
                                        .cornerRadius(15)
                                })
                                
                            }
                        }.padding([.leading], geometry.size.width * 0.03)
                            .frame(height: geometry.size.height * 0.18)
                        
                        // All activities
                        ForEach(activities, id: \.self){ String in
                            
                            let filteredActivities = generalActivities.filter{ activity in
                                return activity.element.type == String
                            }
                            if filteredActivities.count > 0{
                                HStack{
                                    Text(String)
                                        .padding(.leading, 10)
                                        .font(.system(size: 23, weight: .bold))
                                    Spacer()
                                }
                                ScrollView(.horizontal){
                                    LazyHGrid(rows: gridLayout, alignment: .center, spacing: 15) {
                                        ForEach(filteredActivities, id: \.offset) { index, activityI in
                                            let place = IdentifiablePlace(lat: activityI.latitude, long: activityI.longitude)
                                            let region = MKCoordinateRegion(
                                                center: CLLocationCoordinate2D( latitude: activityI.latitude-0.005, longitude: activityI.longitude),
                                                span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
                                            )
                                            NavigationLink(destination: ActivityDetail(activity: activityI, region: region, place: place), label:{
                                                
                                                ActivityCard(height: geometry.size.height, width: geometry.size.width, name: activityI.name, city: activityI.city, URLpic: activityI.imageUrl)
                                                    .frame(minHeight: 0, maxHeight: geometry.size.height * 0.9)
                                                    .frame(width: geometry.size.width * 0.75)
                                                    .padding([.bottom], geometry.size.width * 0.03)
                                                    .cornerRadius(15)
                                                    .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
                                                
                                            } )
                                        }
                                    }
                                }.padding([.leading], geometry.size.width * 0.03)
                                    .frame(height: geometry.size.height * 0.18)
                            }
                        }
                    }
                }
                .coordinateSpace(name: "pullToRefresh")
            }
            
        }.background(Color("Color1"))
    }
}

struct CardModifier: ViewModifier {
    func body(content: Content) -> some View {
        content
            .cornerRadius(15)
            .shadow(color: Color.black.opacity(0.2), radius: 10, x: 10, y: 10)
    }
    
}

struct ActivityCard: View{
    @State var height: CGFloat
    @State var width: CGFloat
    @State var name: String
    @State var city: String
    @State var URLpic: String
    
    
    var body: some View {
        
        HStack(alignment: .center) {
            HStack{
                AsyncImage(
                    url: URL(string: URLpic) ??  URL(string: "placeholder")!,
                    placeholder: { Image("placeholder") },
                    image: { Image(uiImage: $0).resizable()
                    }
                )
                .scaledToFill()
                .frame(width:width * 0.26, height: height * 0.2)
                .clipped()
                
            }
            .frame(width: width * 0.26, height: height * 0.2)
            
            
            VStack(alignment: .leading) {
                Text(name)
                    .font(.system(size: 19, weight: .semibold, design: .default))
                    .foregroundColor(Color(red: 43/255, green: 43/255, blue: 42/250))
                    .padding(.leading, 3)
                HStack{
                    Image("location")
                    Text(city)
                        .font(.system(size: 16, weight: .bold, design: .default))
                        .foregroundColor(.gray)
                }
                
                HStack {
                    Text("")
                        .font(.system(size: 16, weight: .bold, design: .default))
                        .foregroundColor(.gray)
                        .padding(.top, 8)
                }
            }.padding(.trailing, 20)
            Spacer()
        }
        .frame(maxHeight: .infinity, alignment: .center)
        .frame(maxWidth: .infinity, alignment: .center)
        .background(Color(red: 242/255, green: 236/255, blue: 225/250))
        .modifier(CardModifier())
        
    }
    
}


struct PullToRefresh: View {
    
    var coordinateSpaceName: String
    var onRefresh: ()->Void
    
    @State var needRefresh: Bool = false
    
    var body: some View {
        GeometryReader { geo in
            if (geo.frame(in: .named(coordinateSpaceName)).midY > 50) {
                Spacer()
                    .onAppear {
                        needRefresh = true
                    }
            } else if (geo.frame(in: .named(coordinateSpaceName)).maxY < 10) {
                Spacer()
                    .onAppear {
                        if needRefresh {
                            needRefresh = false
                            onRefresh()
                        }
                    }
            }
            HStack {
                Spacer()
                if needRefresh {
                    ProgressView()
                } else {
                    Text("⬇️")
                }
                Spacer()
            }
        }.padding(.top, -50)
    }
}
