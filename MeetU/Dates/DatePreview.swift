//
//  DatePreview.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/1/22.
//

import SwiftUI

struct DatePreview: View {
    @EnvironmentObject var fm: FirestoreManager
    @State var estado: String = ""
    var id: String
    var ubication: String
    var hour: String
    
    
    var body: some View {
        ZStack(alignment: .leading) {
            
            Color("Color2")
            HStack {
                VStack(alignment: .leading) {
                    Text(estado)
                        .font(.headline)
                        .fontWeight(.bold)
                        .lineLimit(2)
                        .padding(.bottom, 5)
                        .foregroundColor(.white)
                    
                    /*  HStack(alignment: .center) {
                     Image(systemName: "mappin")
                     Text(ubication)
                     }
                     .padding(.bottom, 5)
                     .foregroundColor(.white) */
                    HStack {
                        CategoryPill(categoryName: hour)
                        
                    }
                }
                .padding(.horizontal, 5)
            }
            .padding(15)
        }
        .clipShape(RoundedRectangle(cornerRadius: 15))
        .onAppear{
            Task{
                estado = await fm.fetchNameById(userId: id)
            }
        }
    }
}

struct CategoryPill: View {
    
    var categoryName: String
    var fontSize: CGFloat = 12.0
    
    var body: some View {
        ZStack {
            Text(categoryName)
                .font(.system(size: fontSize, weight: .regular))
                .lineLimit(2)
                .foregroundColor(.white)
                .padding(5)
                .background(Color("Color3"))
                .cornerRadius(5)
        }
    }
}
