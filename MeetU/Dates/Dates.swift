//
//  Dates.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/1/22.
//

import SwiftUI

struct Dates: View {
    @EnvironmentObject var user: User
    @EnvironmentObject var fm: FirestoreManager
    @EnvironmentObject var networkState: NetworkMonitor
    let dateFormatter = DateFormatter()
    
    init(){
        dateFormatter.dateFormat = "yyyy/MM/dd"
    }
    var body: some View {
        ZStack{
            Color("Color1")
                .ignoresSafeArea()
            VStack{
                Text("Likes given").foregroundColor(Color("Color3")).font(.title).fontWeight(.heavy)
                ScrollView{
                    ForEach(Array(fm.userFullLikes.likes.enumerated()), id: \.offset) { index, Like in
                        DatePreview(id:fm.userFullLikes.likes[index].0,ubication: "Bogota-Universidad de los Andes",hour: dateFormatter.string(from: fm.userFullLikes.likes[index].1)).padding(.horizontal,20)
                    }
                }
                .navigationBarTitle("")
                .navigationBarBackButtonHidden(true)
                .onAppear{
                    Task{
                        await fm.fetchFullLikes(networkState: networkState.status, userID: user.uID)
                    }
                }
            }
        }
    }
}








