//
//  MeetUApp.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/11/22.
//

import SwiftUI
import Firebase

@main
struct MeetUApp: App {
    let persistenceController = PersistenceController.shared
    let networkMonitor = NetworkMonitor.shared
    
    init() {
      FirebaseApp.configure()
        networkMonitor.start()
    }

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
                .environmentObject(networkMonitor)
        }
    }
}
