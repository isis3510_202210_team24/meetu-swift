//
//  NetworkMonitor.swift
//  MeetU
//
//  Created by Danna Mayorga on 24/04/22.
//

import Foundation
import Network

class NetworkMonitor: ObservableObject {
    
    static let shared = NetworkMonitor()
    private let monitor: NWPathMonitor
    private let queue = DispatchQueue(label: "NetworkMonitorQueue")
    @Published var status: Bool
    @Published var notConnected: Bool
    
    private init() {
        monitor = NWPathMonitor()
        status = true
        notConnected = false
    }
    
    func start() {
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in
            if path.status == .satisfied {
                
                DispatchQueue.main.async {
                    //print("hay conexion")
                    self?.status = true
                    self?.notConnected = false
                }
            }
            else {
                
                DispatchQueue.main.async {
                    //print("no hay conexion")
                    self?.status = false
                    self?.notConnected = true
                }
            }
        }
    }
    
}
