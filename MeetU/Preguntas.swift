//
//  Preguntas.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/24/22.
//

import SwiftUI

struct Preguntas: View {
    var body: some View {
        VStack {
            Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
        }.navigationBarBackButtonHidden(true)
    }
}

struct Preguntas_Previews: PreviewProvider {
    static var previews: some View {
        Preguntas()
    }
}
