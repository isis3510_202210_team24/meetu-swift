//
//  PreferenceView.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/30/22.
//

import SwiftUI

struct PreferenceView: View {
    let preference: Preference
    let add: (String)->Void
    let remove: (String)->Void
    let placeholder = "https://upload.wikimedia.org/wikipedia/commons/thumb/a/ad/Placeholder_no_text.svg/1200px-Placeholder_no_text.svg.png"
    @State private var selected: Bool = false
    
    var body: some View {
        
        Button(action: {
            if selected{
                remove(preference.id)
            }
            else{
                add(preference.id)
            }
            selected.toggle()
        }, label: {
            VStack{
                AsyncImage(
                    url: URL(string: preference.iconUrl) ?? URL(string: placeholder)!,
                    placeholder: { Image("placeholder") },
                    image: { Image(uiImage: $0).resizable()
                    }
                )
                .scaledToFit()
                .transition(.slide)
                .frame(width: 65, height: 60)
                .clipped()
                .shadow(radius: 5)
                .padding()
                Text(preference.name)
                    .font(.headline)
                    .fontWeight(.heavy)
                    .if(selected) { $0.foregroundColor(Color("Color1")) }
                    .foregroundColor(Color("Color2"))
                    .multilineTextAlignment(.center)
                Text(preference.category)
                    .font(.subheadline)
                    .fontWeight(.light)
                    .if(selected) { $0.foregroundColor(Color("Color1")) }
                    .foregroundColor(Color("Color2"))
                    .multilineTextAlignment(.center)
                    .padding(.bottom)
            }
        })
        .if(selected) { $0.background(Color("Color2")) }
        .background(Color("Color1"))
        .cornerRadius(15)
        .if(!selected) { $0.shadow(radius: 7) }
        //.shadow(radius: 5)
    }
}
