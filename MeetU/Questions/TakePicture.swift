//
//  TakePicture.swift
//  MeetU
//
//  Created by Andrés Martínez on 4/3/22.
//

import SwiftUI
import AVFoundation
import Photos

struct TakePicture: View {
    @State var image: Image? = nil
    @State var rawImage: UIImage? = nil
    @State var showCaptureImageView: Bool = false
    @State var source: Bool = false
    @EnvironmentObject var db: FirestoreManager
    @EnvironmentObject var user: User
    var next: (() -> Void)
    var bar: (() -> Void)
    
    func trigger(){
        self.showCaptureImageView.toggle()
    }
    
    func permissionCamera(){
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.trigger()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { granted in
                if granted {
                    self.trigger()
                }
            }
            
        case .denied:
            return
            
        case .restricted:
            return
        @unknown default:
            fatalError()
        }
    }
    
    func permissionLibrary(){
        PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
            switch status {
            case .notDetermined:
                return
            case .restricted:
                return
            case .denied:
                return
            case .authorized:
                self.trigger()
            case .limited:
                self.trigger()
            @unknown default:
                fatalError()
            }
        }
    }
    
    func cameraRoll(){
        self.source = false
        self.permissionLibrary()
    }
    func camera(){
        self.source = true
        self.permissionCamera()
    }
    
    var body: some View {
        ZStack {
            VStack {
                Text("Let's add a picture to your profile!")
                    .font(.title)
                    .fontWeight(.heavy)
                    .foregroundColor(Color("Color2"))
                    .multilineTextAlignment(.center)
                    
                Spacer()
                image?.resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: 270, height: 270)
                    .clipShape(Circle())
                    .overlay(Circle().stroke(Color.white, lineWidth: 4))
                    .shadow(radius: 10)
                    .padding(.bottom, 30.0)
                if image == nil {
                    
                    Button("Take selfie"){
                        camera()
                    }
                    .padding(.bottom, 8)
                    .buttonStyle(NiceButtonStyle())
                    Button("Camera Roll"){
                        cameraRoll()
                    }
                    .padding(.bottom, 8)
                    .buttonStyle(NiceButtonStyle())
                    
                }
                else{
                    if source{
                        Button("Re-take selfie"){
                            camera()
                        }
                        .padding(.bottom, 8)
                        .buttonStyle(NiceButtonStyle())
                        Button("Camera Roll"){
                            cameraRoll()
                        }
                        .padding(.bottom, 8)
                        .buttonStyle(NiceButtonStyle())
                    }
                    else{
                        Button("Take selfie"){
                            camera()
                        }
                        .padding(.bottom, 8)
                        .buttonStyle(NiceButtonStyle())
                        Button("Camera Roll"){
                            cameraRoll()
                        }
                        .padding(.bottom, 8)
                        .buttonStyle(NiceButtonStyle())
                    }
                    Button("Continue"){
                        if let rawImage = rawImage {
                            DispatchQueue.global(qos: .userInitiated).async {
                                db.setImage(user: user.uID, image: rawImage.jpegData(compressionQuality: 0.2)!)
                            }
                        }
                        next()
                    }
                    .buttonStyle(NiceButtonStyle())
                    
                }
                Spacer()
                Button(action: next, label: {Text("Continue without picture")})
                    .padding(.bottom, 15)
            }
            if (showCaptureImageView) {
                CaptureImageView(isShown: $showCaptureImageView, image: $image, rawImage: $rawImage, source: $source)
                    .ignoresSafeArea()
                    .onDisappear{
                        self.bar()
                    }
                    .onAppear{
                        self.bar()
                    }
            }
        }
    }
    
}


struct CaptureImageView {
    @Binding var isShown: Bool
    @Binding var image: Image?
    @Binding var rawImage: UIImage?
    @Binding var source: Bool
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(isShown: $isShown, image: $image, rawImage: $rawImage)
    }
}
extension CaptureImageView: UIViewControllerRepresentable {
    func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        if source{
            picker.sourceType = .camera
        }
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController,
                                context: UIViewControllerRepresentableContext<CaptureImageView>) {
        
    }
}
