//
//  Questions.swift
//  MeetU
//
//  Created by Andrés Martínez on 3/25/22.
//

import SwiftUI

struct Questions: View {
    @EnvironmentObject var networkMonitor: NetworkMonitor
    @EnvironmentObject var db: FirestoreManager
    @EnvironmentObject var user: User
    @State var progress: Int? = 0
    @State var disapear: Bool = true
    @State var answers: [[String]] = []
    @State var noConnection: Bool = false
    @State var preferences: [String] = []
    func next(){
        progress? += 1
    }
    func previous(){
        progress? -= 1
        
        if let progress = progress{
            if progress > 0{
                self.answers.removeLast()
            }
        }
    }
    func disapearBar(){
        self.disapear.toggle()
    }
    func addElement(element: [String]){
        self.answers.append(element)
    }
    func reset(){
        self.disapear = true
        self.answers  = []
        self.progress = 0
        self.noConnection = false
    }
    func addPreference(preference: String){
        self.preferences.append(preference)
    }
    func removePreference(preference: String){
        if let index = preferences.firstIndex(of: preference) {
            preferences.remove(at: index)
        }
    }
    
    var body: some View {
        
        ZStack {
            if let progress = progress{
                Color("Color1")
                    .ignoresSafeArea()
                VStack{
                    if disapear{
                        ProgressView(value: CGFloat(progress+1), total: CGFloat(db.questions.count+2))
                            .tint(Color("Color3"))
                            .padding(.all)
                    }
                    if progress == 0{
                        TakePicture(next: self.next, bar: self.disapearBar)
                    }
                    else if progress <= db.questions.count{
                        let quest = db.questions[progress-1]
                        QuestionView(question:quest.question,answers:quest.options,previous:self.previous,next: self.next, add: addElement)
                        
                    }
                    else{
                        Text("One last thing, pick at least three preferences!")
                            .font(.headline)
                            .fontWeight(.heavy)
                            .foregroundColor(Color("Color2"))
                            .multilineTextAlignment(.center)
                            .padding(.top, 20)
                            .padding([.leading, .bottom, .trailing])
                            .onAppear{
                                self.disapearBar()
                            }
                            .onDisappear{
                                self.disapearBar()
                            }
                        
                        ScrollView(){
                            LazyVGrid(columns: [
                                GridItem(.flexible()),
                                GridItem(.flexible()),
                                GridItem(.flexible())
                            ], spacing: 20) {
                                ForEach(Array(db.preferences.enumerated()), id: \.offset) { index, preference in
                                    PreferenceView(preference: preference, add: addPreference, remove: removePreference).padding(.horizontal,12)
                                }
                            }
                            .padding()
                        }
                        Spacer()
                        
                        if preferences.count>=3{
                            Button("Begin"){
                                if networkMonitor.status{
                                    db.refreshQuestionsStatus(userID: user.uID)
                                    db.setUserQuestions(user: user.uID, questions: self.answers)
                                    db.setUserPreferences(user: user.uID, preferences: self.preferences)
                                    //next()
                                    reset()
                                    user.questions = false
                                    user.updateFullIni()
                                }
                                else{
                                    self.noConnection = true
                                }
                            }
                            //.padding(.bottom, 8)
                            .padding()
                            .buttonStyle(NiceButtonStyle())
                        }
                        
                        
                        if self.noConnection{
                            Text("No connection, try again")
                                .fontWeight(.heavy)
                                .foregroundColor(Color("Color3"))
                        }
                        
                        Spacer()
                        
                        //Button(action: previous, label: {Text("Back")})
                        //   .padding(.bottom, 15)
                        
                    }
                    
                    /*
                     NavigationLink(destination: MainView(), tag: db.questions.count+2, selection: $progress) {
                     EmptyView()
                     }
                     */
                }
            }
        }   .navigationBarTitle("")
            .navigationBarHidden(true)
            .navigationBarBackButtonHidden(true)
        
    }
}

struct NiceButtonStyle: ButtonStyle {
    @Environment(\.isEnabled) private var isEnabled
    
    func makeBody(configuration: Configuration) -> some View {
        configuration
            .label
            .padding()
            .frame(width: 200, height: 45)
            .background(Color("Color3"))
            .clipShape(Capsule())
            .foregroundColor(Color.white)
            .shadow(radius: 5)
    }
}


