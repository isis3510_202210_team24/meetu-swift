//
//  QuestionView.swift
//  MeetU
//
//  Created by Andrés Martínez on 4/3/22.
//

import SwiftUI

struct QuestionView: View {
    @State var selected: [String] = []
    var question: String
    var answers: [String]
    var previous: ()->Void
    var next: ()->Void
    var add:([String])->Void

    
    var body: some View {
        VStack {
            Text(question)
                .font(.title)
                .fontWeight(.heavy)
                .foregroundColor(Color("Color2"))
                .multilineTextAlignment(.center)
                .padding(.all)
            Spacer()
            ForEach(answers, id:\.self){ answer in
                Button(answer){
                    self.selected = [self.question,answer]
                    add(selected)
                    next()
                }
                .padding(.bottom, 8)
                .buttonStyle(NiceButtonStyle())
            }
            Spacer()
            Button(action: previous, label: {Text("Back")})
                .padding(.bottom, 15)
        }
        
    }
}


