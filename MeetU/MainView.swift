//
//  MainView.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/1/22.
//

import SwiftUI

struct MainView: View {
    
    @EnvironmentObject private var db: FirestoreManager
    @EnvironmentObject private var networkState: NetworkMonitor
    @EnvironmentObject private var user: User
    
    @State var selectedView: Int = 1
    
    var body: some View {
        VStack {
            ZStack{
                Explore()
                if selectedView == 3{
                    //ActivityDetail()
                }
                else if selectedView == 0{
                    
                    ProfileView()
                }
                else if selectedView == 2 {
                    ActivityList()
                }
            }
            TabBar(selectedIndex: $selectedView)
        }
        .background(Color("Color1"))
        .onAppear{
            print("appear main")
            DispatchQueue.global(qos: .userInitiated).async {
                Task{
                    await db.fetchFullProfile(networkState: networkState.status, userID: user.uID)
                    await db.fetchActivities(networkState: networkState.status)
                    await db.fetchUserLikes(userId: user.uID)
                    await db.fetchUserLikeCount(networkState: networkState.status, userId: user.uID)
                    await db.fetchFullLikes(networkState: networkState.status, userID: user.uID)
                }
            }
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
    }
}

