//
//  ProfileView.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/1/22.
//

import SwiftUI

struct ProfileView: View {
    @EnvironmentObject private var db: FirestoreManager
    @EnvironmentObject private var networkState: NetworkMonitor
    @EnvironmentObject private var user: User
    @State private var profileImage: Image?
    var dateFormatter = DateFormatter()
    init() {
        UITableView.appearance().separatorStyle = .none
        UITableViewCell.appearance().backgroundColor = UIColor(Color("Color1"))
        UITableView.appearance().backgroundColor = UIColor(Color("Color1"))
        UITableView.appearance().contentInset.top = -30
        dateFormatter.dateFormat = "yyyy/MM/dd"
    }
    func likesPast(likes:Int) -> String{
        if likes > 5{
            return String(likes)
        }
        else{
            return String(likes) + " (You should give more likes)"
        }
    }
    var body: some View {
        ZStack {
            Color("Color1")
                .ignoresSafeArea()
            VStack{
                List{
                    HStack{
                        Spacer()
                        if let loadedImage = profileImage {
                            loadedImage
                                .resizable()
                                .aspectRatio(contentMode: .fill)
                                .transition(.slide)
                                .frame(width: 180, height: 180)
                                .background(Color("Color1"))
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                                .shadow(radius: 10)
                                .padding()
                        }else{
                            ProgressView()
                                .frame(width: 180, height: 180)
                                .background(Color("Color1"))
                                .clipShape(Circle())
                                .overlay(Circle().stroke(Color.white, lineWidth: 4))
                                .shadow(radius: 10)
                                .padding()
                                .onAppear{
                                    DispatchQueue.global(qos: .userInitiated).async {
                                        let url = getDocumentsDirectory().appendingPathComponent("profile.jpeg")
                                        let data = try? Data(contentsOf: url)
                                        var imagenSeleccionada = Image("placeholder")
                                        if data != nil{
                                            //print("imagenEncontrada")
                                            let image = UIImage(data: data!)
                                            imagenSeleccionada = Image(uiImage: image!)
                                            let modified = fileModificationDate(url: url)
                                            if modified != nil{
                                                let diffComponents = Calendar.current.dateComponents([.hour], from: modified!, to: Date()).hour
                                                if let hours = diffComponents {
                                                    if hours > 1{
                                                        if networkState.status{
                                                            if let webURL = URL(string: db.userProfile.photoUrl){
                                                                let data = try? Data(contentsOf: webURL)
                                                                if data != nil{
                                                                    //print("imagenDescargada")
                                                                    let image = UIImage(data: data!)
                                                                    let imageData = image?.jpegData(compressionQuality: 1)
                                                                    imagenSeleccionada = Image(uiImage: image!)
                                                                    do {
                                                                        try imageData!.write(to: url)
                                                                        //let input = try String(contentsOf: url)
                                                                        //print("ruta")
                                                                        //print(input)
                                                                    } catch {
                                                                        print(error.localizedDescription)
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else{
                                            //print("imagenNoEncontrada")
                                            if networkState.status{
                                                //print("HayredImagen")
                                                if let webURL = URL(string: db.userProfile.photoUrl){
                                                    let data = try? Data(contentsOf: webURL)
                                                    //print("imagenData")
                                                    if data != nil{
                                                        //print("imagenDescargada")
                                                        let image = UIImage(data: data!)
                                                        let imageData = image?.jpegData(compressionQuality: 1)
                                                        imagenSeleccionada = Image(uiImage: image!)
                                                        do {
                                                            try imageData!.write(to: url)
                                                            //let input = try String(contentsOf: url)
                                                            //print("ruta")
                                                            //print(input)
                                                        } catch {
                                                            print(error.localizedDescription)
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        DispatchQueue.main.async {
                                            profileImage = imagenSeleccionada
                                        }
                                    }
                                }
                        }
                        Spacer()
                    }
                    .listRowBackground(Color("Color1"))
                    
                    Section(header: Text("Profile").foregroundColor(Color("Color3")).font(.headline)) {
                        ProfileInfoCard(title:db.userProfile.name, subTitle:"Name")
                            .listRowBackground(Color("Color1"))
                        ProfileInfoCard(title:db.userProfile.lastName, subTitle:"Last name")
                            .listRowBackground(Color("Color1"))
                        ProfileInfoCard(title:db.userProfile.email, subTitle:"Email")
                            .listRowBackground(Color("Color1"))
                        ProfileInfoCard(title: dateFormatter.string(from: db.userProfile.birthdate ?? Date()), subTitle:"Birthdate")
                            .listRowBackground(Color("Color1"))
                        ProfileInfoCard(title: String(db.likeCount ?? 0), subTitle:"Total likes recieved")
                            .listRowBackground(Color("Color1"))
                        ProfileInfoCard(title: String(db.userFullLikes.numberOfLikes()), subTitle:"Total likes given")
                            .listRowBackground(Color("Color1"))
                        ProfileInfoCard(title: likesPast(likes: db.userFullLikes.likesInPastWeek()), subTitle:"Likes given in the past week")
                            .listRowBackground(Color("Color1"))
                    }
                }
                .environment(\.defaultMinListRowHeight, 0)
                .environment(\.defaultMinListHeaderHeight, 0)
                .foregroundColor(Color("Color1"))
                .background(Color("Color1"))
                .refreshable{
                    Task{
                        await db.fetchFullProfile(networkState: networkState.status, userID: user.uID)
                        await db.fetchFullLikes(networkState: networkState.status, userID: user.uID)
                        await db.fetchUserLikeCount(networkState: networkState.status, userId: user.uID)
                    }
                }
                .padding(.bottom, 8)
                Spacer()
                Button("LogOut"){
                    DispatchQueue.global(qos: .default).async {
                        do {
                            try FileManager.default.removeItem(at: getDocumentsDirectory().appendingPathComponent("profile.jpeg"))
                        } catch  {
                            //print(error)
                        }
                        
                    }
                    user.uID = ""
                    user.email = ""
                    db.logOut()
                    user.questions = false
                    user.ini = false
                    //print("logOutTermino")
                }
                .padding(.bottom, 8)
                .buttonStyle(NiceButtonStyle())
            }
            //.padding(.top,8)
        }
        .navigationBarTitle("")
        .navigationBarHidden(true)
        .navigationBarBackButtonHidden(true)
        .background(Color("Color1"))
    }
}

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    return paths[0]
}

func fileModificationDate(url: URL) -> Date? {
    do {
        let attr = try FileManager.default.attributesOfItem(atPath: url.path)
        return attr[FileAttributeKey.modificationDate] as? Date
    } catch {
        return nil
    }
}
