//
//  ProfileInfoCard.swift
//  MeetU
//
//  Created by Andrés Martínez on 5/5/22.
//

import SwiftUI

struct ProfileInfoCard: View {
    var title:String
    var subTitle:String
    var body: some View {
        
        VStack(alignment: .leading) {
            Text(title)
                .font(.headline)
            Spacer()
            Text(subTitle)
                .font(.caption)
        }
        .padding()
        .foregroundColor(Color("Color2"))
    }
}
